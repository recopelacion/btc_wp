<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
		<?php
		bloginfo('name');
		echo ' | ';
        if (wp_title('', false)) {
			echo "";
        } else {
            echo bloginfo('description');
        } wp_title('');
		?>
	</title>
    <link rel="icon" href="<?php echo get_template_directory_uri()?>/assets/img/favicon.ico">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if( is_singular() && pings_open( get_queried_object() ) ): ?>
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<script src="<?php echo get_template_directory_uri()?>/assets/js/jquery.min.js"></script>
	<script>
		jQuery(function($){
			//Modal
			var $p_lists = $('.floor-list'),
				$p_tit = "",
				$img_src = "",
				$modal = $('.company-modal'),
				$modal_close = $('.modal-close'),
				$p_slider = $('#modal-carousel'),
				$item_count = 0;

			$p_lists.find('li').each(function(){
				$(this).find('a').attr('data-count', $(this).index());
				$p_slider.append("<div class='item'><img class='is-wide' src='"+$(this).find('img').attr('src')+"'></div>")
			});

			$('#modal-carousel').owlCarousel({
				loop:false,
				margin:0,
				center: true,
				responsiveClass:true,
				nav:true,
				items:1,
				mouseDrag: true,
				touchDrag: true,
				pullDrag: true
			});
				
			$p_lists.find('li').children('a').click(function(e){
				e.preventDefault();
				$img_src = $(this).find('img').attr('src');
				$item_count = parseInt($(this).attr('data-count'))+1; 
				$modal.fadeIn(500).css({
					display: 'flex'
				});	
				$('#modal-carousel').trigger("to.owl.carousel", [$item_count, 1, true]);

				return false;
			});

			$modal_close.click(function(e){
				e.preventDefault();
				$modal.fadeOut(100).css({
					display: 'flex'
				});	
				return false;
			});	
		});
	</script>
	<script src="<?php echo get_template_directory_uri()?>/assets/js/owl.carousel.js"></script>
	<script src="<?php echo get_template_directory_uri()?>/assets/js/owl.lazyload.js"></script>
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

	<div class="main-menu">
		<div class="cntr">
			<!-- head -->
	<div class="head">
		<header class="main-header">
			<a href="javascript:;" id="toggle-nav">
				<span></span>
				<span></span>
				<span></span>
			</a>
			<h1 class="logo">
				<a href="<?php bloginfo('url'); ?>">
					<img src="<?php echo get_template_directory_uri()?>/assets/img/logo01.png" alt="btc-logo" class="is-wide">
				</a>
			</h1>
		</header>
		<!-- nav -->
		<nav class="main-nav">
			<div class="lang-area">
				<ul>
					<li>
						<a href="#" class="is-active">JP</a>
					</li>
					<li>
						<a href="#">EN</a>
					</li>
					<li>
						<a href="#">VN</a>
					</li>
				</ul>
			</div>
			<div class="nav-links-area">
				<?php 
					wp_nav_menu(
						array (
							'theme_location' => 'primary',
							'walker'         => new WPSE_78121_Sublevel_Walker
							)
						); 
				?>
				<div class="search-bar">
					<input type="search" class="input-search" placeholder="Search">
					<button type="submit" class="search-btn">
						<img src="<?php echo get_template_directory_uri()?>/assets/img/ico_search01.png" alt="search">
					</button>
				</div>
			</div>
		</nav>
		<!-- //nav -->
	</div>
	<!-- //head -->
		</div>
	</div>

	<!-- search -->
	<div id="search-container" class="wrp search-wrp">
	<div class="overlay">
			<div class="cntr">
				<div id="search-inner">
					<div class="search-form">
						<button type="button" class="close">×</button>
						<form role="search" method="get" action="<?php echo home_url('/'); ?>">
							<input type="text" class="field search" name="s" id="keyword" placeholder="Start Typing..." value="<?php the_search_query(); ?>" onkeyup="fetch()">
							<input type="hidden" name="post_type" value="post" />
						</form>
					</div>
					<div id="datafetch"></div>
				</div>
			</div>
		</div>
	</div>
	<!-- //search -->