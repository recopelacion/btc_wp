<?php
	/***
		Template Name: Homepage
	***/
?>
<?php get_header(); ?>

	<!-- hero -->
	<section class="wrp">
		<div class="hero">
			<div class="main-slider-box">
				<?php
					if ( get_query_var('paged') ) { $paged = get_query_var('paged'); } else if ( get_query_var('page') ) {$paged = get_query_var('page'); } else {$paged = 1; }
					$temp = $wp_query;
					$wp_query = null;
					$args = array( 'post_type' => 'slider', 'order'=>'DESC', 'posts_per_page' => 3, 'paged' => $paged);
					$wp_query = new WP_Query();
					$wp_query->query( $args );
					while ($wp_query->have_posts()) : $wp_query->the_post();
				?>
				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<?php
						$thumb_id = get_post_thumbnail_id(get_the_ID());
						$alt = get_post_meta($thumb_id, '_wp_attachment_image_alt', true);
					?>
					<img title="<?php the_title(); ?>" alt="<?php echo $alt; ?>" class="wp-post-image is-wide v-pc" src="<?=wp_get_attachment_url( get_post_thumbnail_id() ); ?>">
					<img src="<?php echo get_post_meta($post->ID,'slider_img', true); ?>" class="wp-post-image is-wide v-sp" alt="">
				</div>
				<?php endwhile; ?>
			</div>
		</div>
	</section>
	<!-- //hero -->
	
	<!-- news-section -->
	<section class="wrp sec bg-gray sec-news ov-hidden">
		<div class="cntr plr-0-xs">
			<h2 class="title">
				<em>NEWS</em>
				<span>ニュース</span>
			</h2>
			<div class="gap gap-10 gap-0-xs news" id="news-section">
				<?php
					if ( get_query_var('paged') ) { $paged = get_query_var('paged'); } else if ( get_query_var('page') ) {$paged = get_query_var('page'); } else {$paged = 1; }
					$temp = $wp_query;
					$wp_query = null;
					$args = array( 'post_type' => 'post', 'order'=>'DESC', 'posts_per_page' => 3, 'paged' => $paged);
					$wp_query = new WP_Query();
					$wp_query->query( $args );
					while ($wp_query->have_posts()) : $wp_query->the_post();
				?>

					<div id="post-<?php the_ID(); ?>" <?php post_class('md-4 xs-12 item'); ?> data-aos="fade-up" data-aos-duration="2000">
						<a href="<?php the_permalink(); ?>" class="card">
							<div class="card-body">
								<div class="card-img">
									<?php
										$thumb_id = get_post_thumbnail_id(get_the_ID());
										$alt = get_post_meta($thumb_id, '_wp_attachment_image_alt', true);
									?>
									<img title="<?php the_title(); ?>" alt="<?php echo $alt; ?>" class="wp-post-image is-wide" src="<?=wp_get_attachment_url( get_post_thumbnail_id() ); ?>">
								</div>
								<div class="card-infos">
									<div class="card-date">
										<?php the_time('Y.m.d'); ?>
									</div>
									<div class="card-title">
										<h3>
											<!-- <?php //echo mb_strimwidth(get_the_title(), 0, 50, ''); ?> -->
											<?php the_title(); ?>
										</h3>
									</div>
								</div>
							</div>
						</a>
					</div>

				<?php endwhile; ?>
			</div>
			<div class="cntr tr">
				<a href="#" class="btn-readmore">
					READ MORE
				</a>
			</div>
		</div>
	</section>
	<!-- //news-section -->

	<!-- column-section -->
	<section class="wrp sec sec-column ov-hidden">
		<div class="cntr">
			<h2 class="title">
				<em>Core Technology</em>
				<span>コアテクノロジー</span>
			</h2>
			<h3 class="sub-infos tc">
				テクノロジーが持つ可能性を最大限に引き出し、<br class="v-sp">ビジネスの発展を加速させる。<br>
				これが、BTCが提供するコンサルティングの強みです。
			</h3>
			<div class="column-category">
				<div class="gap gap-40 gap-0-xs">
					<div class="md-6 xs-12 mb-50 mb-30-xs" data-aos="fade-right" data-aos-duration="2000">
						<div class="card has-slider">
							<div class="card-header">
								<div class="category">
									<h4 class="cat-tit is-cloud tc">
										<span> <img src="<?php echo get_template_directory_uri()?>/assets/img/ico_cloud.png" alt=""> Cloud</span>
									</h4>
									<div class="cat-info tr">
										<p class="tc">
											近年の急激な情報技術の進歩に伴い、製品や技術要素の<br class="v-pc">
											選択肢は増加の一途を辿っています。
										</p>
										<a href="#" class="link-details">詳しくはこちら &gt;</a>
									</div>
								</div>
							</div>
							<div class="card-body">
								<div class="column-slider owl-theme owl-carousel">
									<?php
										global $post;
										$args = array(
											'paged' => $paged,
											'posts_per_page' => 3, 
											'orderby' => 'date', 
											'order' => 'DESC', 
											'post_type' => 'core-technology',
											'tax_query' => array(
												array(
														'taxonomy' => 'core-technology_cat',
														'field' => 'slug',
														'terms' => 'cloud'
												),
											),
										);
										$my_query = new WP_Query($args);
										$max_num_pages = $my_query->max_num_pages;
									?>
									<?php if( $my_query -> have_posts() ) : while($my_query -> have_posts()) : $my_query -> the_post(); ?>
										<a href="<?php the_permalink(); ?>">
										<div class="item">
											<div class="item-img">
												<?php
													$thumb_id = get_post_thumbnail_id(get_the_ID());
													$alt = get_post_meta($thumb_id, '_wp_attachment_image_alt', true);
												?>
												<img title="<?php the_title(); ?>" alt="<?php echo $alt; ?>" class="wp-post-image is-wide" src="<?=wp_get_attachment_url( get_post_thumbnail_id() ); ?>">
											</div>
											<div class="item-infos">
												<div class="card-title">
													<h3>
														<?php the_title(); ?>
													</h3>
												</div>
												<div class="card-date">
													<span><?php the_time('Y.m.d'); ?></span>
												</div>
											</div>
										</div>
										</a>
									<?php endwhile; endif; ?>
								</div>
							</div>
						</div>
					</div>
					<div class="md-6 xs-12 mb-50 mb-30-xs" data-aos="fade-left" data-aos-duration="2000">
						<div class="card has-slider">
							<div class="card-header">
								<div class="category">
									<h4 class="cat-tit is-dm tc">
										<span><img src="<?php echo get_template_directory_uri()?>/assets/img/ico_dm.png" alt="">Digital Marketing</span>
									</h4>
									<div class="cat-info tr">
										<p class="tc">
											近年の急激な情報技術の進歩に伴い、製品や技術要素の<br class="v-pc">
											選択肢は増加の一途を辿っています。
										</p>
										<a href="#" class="link-details">詳しくはこちら &gt;</a>
									</div>
								</div>
							</div>
							<div class="card-body">
								<div class="column-slider owl-theme owl-carousel">
									<?php
										global $post;
										$args = array(
											'paged' => $paged,
											'posts_per_page' => 3, 
											'orderby' => 'date', 
											'order' => 'DESC', 
											'post_type' => 'core-technology',
											'tax_query' => array(
												array(
														'taxonomy' => 'core-technology_cat',
														'field' => 'slug',
														'terms' => 'digital-marketing'
												),
											),
										);
										$my_query = new WP_Query($args);
										$max_num_pages = $my_query->max_num_pages;
									?>
									<?php if( $my_query -> have_posts() ) : while($my_query -> have_posts()) : $my_query -> the_post(); ?>
										<a href="<?php the_permalink(); ?>">
										<div class="item">
											<div class="item-img">
												<?php
													$thumb_id = get_post_thumbnail_id(get_the_ID());
													$alt = get_post_meta($thumb_id, '_wp_attachment_image_alt', true);
												?>
												<img title="<?php the_title(); ?>" alt="<?php echo $alt; ?>" class="wp-post-image is-wide" src="<?=wp_get_attachment_url( get_post_thumbnail_id() ); ?>">
											</div>
											<div class="item-infos">
												<div class="card-title">
													<h3>
														<?php the_title(); ?>
													</h3>
												</div>
												<div class="card-date">
													<span><?php the_time('Y.m.d'); ?></span>
												</div>
											</div>
										</div>
										</a>
									<?php endwhile; endif; ?>
								</div>
							</div>
						</div>
					</div>
					<div class="md-6 xs-12 mb-0 mb-30-xs" data-aos="fade-right" data-aos-duration="2000">
							<div class="card has-slider">
								<div class="card-header">
									<div class="category">
										<h4 class="cat-tit is-ai tc">
											<span><img src="<?php echo get_template_directory_uri()?>/assets/img/ico_ai.png" alt="">AI</span>
										</h4>
										<div class="cat-info tr">
											<p class="tc">
												近年の急激な情報技術の進歩に伴い、製品や技術要素の<br class="v-pc">
												選択肢は増加の一途を辿っています。
											</p>
											<a href="#" class="link-details">詳しくはこちら &gt;</a>
										</div>
									</div>
								</div>
								<div class="card-body">
									<div class="column-slider owl-theme owl-carousel">
										<?php
											global $post;
											$args = array(
												'paged' => $paged,
												'posts_per_page' => 3, 
												'orderby' => 'date', 
												'order' => 'DESC', 
												'post_type' => 'core-technology',
												'tax_query' => array(
													array(
															'taxonomy' => 'core-technology_cat',
															'field' => 'slug',
															'terms' => 'ai'
													),
												),
											);
											$my_query = new WP_Query($args);
											$max_num_pages = $my_query->max_num_pages;
										?>
										<?php if( $my_query -> have_posts() ) : while($my_query -> have_posts()) : $my_query -> the_post(); ?>
											<a href="<?php the_permalink(); ?>">
											<div class="item">
												<div class="item-img">
													<?php
														$thumb_id = get_post_thumbnail_id(get_the_ID());
														$alt = get_post_meta($thumb_id, '_wp_attachment_image_alt', true);
													?>
													<img title="<?php the_title(); ?>" alt="<?php echo $alt; ?>" class="wp-post-image is-wide" src="<?=wp_get_attachment_url( get_post_thumbnail_id() ); ?>">
												</div>
												<div class="item-infos">
													<div class="card-title">
														<h3>
															<?php the_title(); ?>
														</h3>
													</div>
													<div class="card-date">
														<span><?php the_time('Y.m.d'); ?></span>
													</div>
												</div>
											</div>
											</a>
										<?php endwhile; endif; ?>
									</div>
								</div>
							</div>
					</div>
					<div class="md-6 xs-12" data-aos="fade-left" data-aos-duration="2000">
						<div class="card has-slider">
							<div class="card-header">
								<div class="category">
									<h4 class="cat-tit is-rpa tc">
										<span><img src="<?php echo get_template_directory_uri()?>/assets/img/ico_rpa.png" alt="">RPA</span>
									</h4>
									<div class="cat-info tr">
										<p class="tc">
											近年の急激な情報技術の進歩に伴い、製品や技術要素の<br class="v-pc">
											選択肢は増加の一途を辿っています。
										</p>
										<a href="#" class="link-details">詳しくはこちら &gt;</a>
									</div>
								</div>
							</div>
							<div class="card-body">
								<div class="column-slider owl-theme owl-carousel">
									<?php 
									include_once( ABSPATH . WPINC . '/feed.php' );
									$rss = fetch_feed( 'https://rpa.bigtreetc.com/column/feed/' );
									$maxitems = 0;
									if ( ! is_wp_error( $rss ) ) : // Checks that the object is created correctly
 
									// Figure out how many total items there are, but limit it to 5. 
									$maxitems = $rss->get_item_quantity( 3 ); 
									
									// Build an array of all the items, starting with element 0 (first element).
									$rss_items = $rss->get_items( 0, $maxitems );
									// var_dump($rss_items);
									endif;
									?>
									<?php if ( $maxitems == 0 ) : ?>
											<ul><li><?php _e( 'No items', 'my-text-domain' ); ?></li></ul>
									<?php else : ?>
									<?php // Loop through each feed item and display each item as a hyperlink. ?>
									<?php foreach ( $rss_items as $item ) :  ?>
									<?php $img_src = explode(",", $item->get_description())[5]; ?>
									<a href="<?php echo esc_url( $item->get_permalink() ); ?>" title="<?php echo esc_html( $item->get_title() ); ?>" target="_blank">
									<div class="item">
										<div class="item-img">
											<img src="<?php echo $img_src; ?>">
										</div>
										<div class="item-infos">
											<div class="card-title">
												<h3>
													<?php echo esc_html( $item->get_title() ); ?>
												</h3>
											</div>
											<div class="card-date">
												<span><?php echo esc_html( $item->get_date('Y.m.d') ); ?></span>
											</div>
										</div>
									</div>
									</a>
									<?php endforeach; ?>
									<?php endif; ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- //column-section -->

	<!-- recruit-section -->
	<a href="https://recruit.bigtreetc.com/" target="_blank" class="recruit-href">
	<section class="wrp sec sec-recruit ov-hidden"  data-aos="fade-in" data-aos-duration="2000">
		
			<div class="w-1000 mlr-auto">
				<div class="gap gap-10 gap-0-xs">
					<div class="md-6 xs-12">
						<h2 class="title twhite tl is-typeC">
							<em>RECRUIT</em>
							<span>採 用</span>
						</h2>
					</div>
					<div class="md-6 xs-12 twhite">
						<h3 class="title is-typeB tl twhite">
							ビッグツリーテクノロジー＆コンサルティング
						</h3>
						<p>
							設立以来、技術力とコンサルティング力でクライアント
							のビジネスを支え、事業を拡大し続けている当社で一
							流の知識・スキルを磨いていきませんか。
							自分次第で活躍のフィールドを広げていくことができます。
						</p>
					</div>
				</div>
			</div>
	</section>
	</a>
	<!-- //recruit-section -->

<?php get_footer(); ?>