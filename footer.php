	<!-- //recruit-section -->
	<div class="scroll-top tc v-pc">
		<a href="javascript:;" id="toTop"></a>
	</div>
	<footer>
		<div class="cntr">
			<div class="gap gap-10 gap-0-xs ai-c">
				<div class="md-6 xs-12">
					<div class="ft-l">
						<h3 class="logo">
							<a href="<?php bloginfo('url'); ?>">
								<img src="<?php echo get_template_directory_uri()?>/assets/img/logo01.png" alt="btc" class="is-wide">
							</a>
						</h3>
						<p>
							〒106-0041<br class="v-pc">
							東京都港区麻布台2丁目3番5号　ノアビル3階<br>
							TEL： <a href="tel:0351146171" class="footer-phone">03-5114-6171</a>
						</p>
						<ul class="policy-menu">
							<li>
								<a href="<?php bloginfo('url'); ?>/security-policy">セキュリティポリシー</a>
							</li>
							<li>
								<a href="<?php bloginfo('url'); ?>/privacy-policy">プライバシーポリシー</a>
							</li>
						</ul>
					</div>
				</div>
				<div class="md-3 xs-12 twhite">
					<div class="ft-r">
						<ul class="footer-links">
							<li>
								<a href="#">
									<span>トップページ</span>
								</a>
							</li>
							<li>
								<a href="#">
									<span>サービス</span>
								</a>
							</li>
							<li>
								<a href="#">
									<span>事業紹介</span>
								</a>
							</li>
						</ul>
					</div>
				</div>
				<div class="md-3 xs-12 twhite">
					<div class="ft-r">
						<ul class="footer-links">
							<li>
								<a href="<?php bloginfo('url'); ?>/company">
									<span>会社概要</span>
								</a>
							</li>
							<li>
								<a href="#">
									<span>採用情報</span>
								</a>
							</li>
							<li>
								<a href="#">
									<span>お問い合わせ</span>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="copyright twhite tc">
			<div class="cntr">
				<p>
					Copyright &copy; BTC Corporation All Rights Reserved.
				</p>
			</div>
		</div>
	</footer>

<?php wp_footer(); ?>
</body>
</html>