<?php
	/***
		Template Name: Blog
	***/
?>
<?php get_header(); ?>

<section class="wrp sec-blog">
        <div class="page-title titlev2">
            <div class="cntr">
                <h2>
                    Blog <span>ブログ</span>
                </h2>
            </div>
        </div>
        <div class="breadcrumbs">
            <div class="cntr">
                <ul>
                    <li>
                        <a href="<?php bloginfo('url'); ?>">Home</a>
                    </li>
                    <li>
                        <span>ブログ</span>
                    </li>
                </ul>
            </div>
        </div>
        <div class="blog-menu">
            <div class="cntr">
                <?php 
					wp_nav_menu(
						array (
							'theme_location' => 'fourth',
							'walker'         => new WPSE_78121_Sublevel_Walker
							)
						); 
				?>
            </div>
        </div>
        <div class="blog-box">
            <div class="cntr">
                <div class="blog-wrap">
                    <h2 class="title">
                        <em>BLOG</em>
                        <span>全て</span>
                    </h2>
                    <div class="blog-cont">
                        <div class="gap gap-30 gap-0-xs">
                            <div class="md-8 xs-12">
                                <div class="gap gap-10 gap-0-xs">
                                    <?php
                                    if ( get_query_var('paged') ) { $paged = get_query_var('paged'); } else if ( get_query_var('page') ) {$paged = get_query_var('page'); } else {$paged = 1; }
                                    $temp = $wp_query;
                                    $wp_query = null;
                                    $args = array( 'post_type' => 'blog', 'order'=>'DESC', 'posts_per_page' => -1, 'paged' => $paged);
                                    $wp_query = new WP_Query();
                                    $wp_query->query( $args );
                                    while ($wp_query->have_posts()) : $wp_query->the_post();
                                ?>

                                    <div id="post-<?php the_ID(); ?>" <?php post_class('md-6 xs-12 item'); ?>>
                                        <a href="<?php the_permalink(); ?>">
                                            <div class="card card-blog">
                                                <div class="card-header blog-header">
                                                     <?php
                                                        $thumb_id = get_post_thumbnail_id(get_the_ID());
                                                        $alt = get_post_meta($thumb_id, '_wp_attachment_image_alt', true);
                                                    ?>
                                                    <img title="<?php the_title(); ?>" alt="<?php echo $alt; ?>" class="wp-post-image is-wide" src="<?=wp_get_attachment_url( get_post_thumbnail_id() ); ?>">
                                                </div>
                                                <div class="card-body blog-body">
                                                    <div class="card-date blog-date">
                                                        <h4><?php the_time('Y.m.d'); ?> <span class="category">
                                                            <?php
                                                                    $terms = get_the_terms( $post->ID , 'blog_cat' );

                                                                    foreach ( $terms as $term ) {
                                                                    
                                                                    echo $term->name;
                                                                    
                                                                    }
                                                                ?>
                                                        </span></h4>
                                                    </div>
                                                    <div class="card-title blog-title">
                                                        <h3><?php the_title(); ?></h3>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>

                                <?php endwhile; ?>
                                </div>
                                <div class="pagination">
                                    <ul>
                                        <li class="prev">
                                            <a href="#"><</a>
                                        </li>
                                        <li class="active">
                                            <a href="#">1</a>
                                        </li>
                                        <li>
                                            <a href="#">2</a>
                                        </li>
                                        <li>
                                            <a href="#">3</a>
                                        </li>
                                        <li>
                                            <a href="#">4</a>
                                        </li>
                                        <li>
                                            <a href="#">5</a>
                                        </li>
                                        <li class="dots">
                                            <a href="#">...</a>
                                        </li>
                                        <li>
                                            <a href="#">12</a>
                                        </li>
                                        <li class="next">
                                            <a href="#">></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="md-4 xs-12">
                                <?php get_sidebar(); ?>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </section>

<?php get_footer(); ?>