<?php 

/**
 * The template for displaying sample page	
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package btc
 * Template Name: Cloud
 */

get_header(); ?>

<section class="wrp sec-blog">
        <div class="page-title titlev2">
            <div class="cntr">
                <h2>
                    Core Technology <span>ブログ</span>
                </h2>
            </div>
        </div>
        <div class="breadcrumbs">
            <div class="cntr">
                <ul>
                    <li>
                        <a href="<?php bloginfo('url'); ?>">Home</a>
                    </li>
                    <li>
                        <span>ブログ</span>
                    </li>
                </ul>
            </div>
        </div>
        <div class="blog-menu">
            <div class="cntr">
                <?php 
					wp_nav_menu(
						array (
							'theme_location' => 'fourth',
							'walker'         => new WPSE_78121_Sublevel_Walker
							)
						); 
				?>
            </div>
        </div>
        <div class="blog-box">
            <div class="cntr">
                <div class="blog-wrap">
                    <h2 class="title">
                        <em>Core Technology</em>
                        <span><?php the_title(); ?></span>
                    </h2>
                    <div class="blog-cont">
                        <div class="gap gap-30 gap-0-xs">
                            <div class="md-8 xs-12">
                                <div class="gap gap-10 gap-0-xs">
                                <?php
										global $post;
										$args = array(
											'paged' => $paged,
											'posts_per_page' => -1, 
											'orderby' => 'date', 
											'order' => 'DESC', 
											'post_type' => 'core-technology',
											'tax_query' => array(
												array(
														'taxonomy' => 'core-technology_cat',
														'field' => 'slug',
														'terms' => 'cloud'
												),
											),
										);
										$my_query = new WP_Query($args);
										$max_num_pages = $my_query->max_num_pages;
									?>
									<?php if( $my_query -> have_posts() ) : while($my_query -> have_posts()) : $my_query -> the_post(); ?>
                                        <div id="post-<?php the_ID(); ?>" <?php post_class('md-6 xs-12 item'); ?>>
                                            <a href="<?php the_permalink(); ?>">
                                                <div class="card card-blog">
                                                    <div class="card-header blog-header">
                                                        <?php
                                                            $thumb_id = get_post_thumbnail_id(get_the_ID());
                                                            $alt = get_post_meta($thumb_id, '_wp_attachment_image_alt', true);
                                                        ?>
                                                        <img title="<?php the_title(); ?>" alt="<?php echo $alt; ?>" class="wp-post-image is-wide" src="<?=wp_get_attachment_url( get_post_thumbnail_id() ); ?>">
                                                    </div>
                                                    <div class="card-body blog-body">
                                                        <div class="card-date blog-date">
                                                            <h4><?php the_time('Y.m.d'); ?> <span class="category">
                                                                <?php
                                                                    $terms = get_the_terms( $post->ID , 'core-technology_cat' );

                                                                    foreach ( $terms as $term ) {
                                                                    
                                                                    echo $term->name;
                                                                    
                                                                    }
                                                                ?>
                                                            </span></h4>
                                                        </div>
                                                        <div class="card-title blog-title">
                                                            <h3><?php the_title(); ?></h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
									<?php endwhile; endif; ?>
                                </div>
                                <div class="pagination">
                                    <ul>
                                        <li class="prev">
                                            <a href="#"><</a>
                                        </li>
                                        <li class="active">
                                            <a href="#">1</a>
                                        </li>
                                        <li>
                                            <a href="#">2</a>
                                        </li>
                                        <li>
                                            <a href="#">3</a>
                                        </li>
                                        <li>
                                            <a href="#">4</a>
                                        </li>
                                        <li>
                                            <a href="#">5</a>
                                        </li>
                                        <li class="dots">
                                            <a href="#">...</a>
                                        </li>
                                        <li>
                                            <a href="#">12</a>
                                        </li>
                                        <li class="next">
                                            <a href="#">></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="md-4 xs-12">
                                <?php get_sidebar(); ?>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </section>

<?php get_footer(); ?>