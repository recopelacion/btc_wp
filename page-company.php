<?php
	/***
		Template Name: Company
	***/
?>
<?php get_header(); ?>

    <section class="wrp sec-company">
        <div class="page-title titlev1">
            <div class="cntr">
                <h2>
                    Company <span>会社概要</span>
                </h2>
            </div>
        </div>
        <div class="breadcrumbs">
            <div class="cntr">
                <ul>
                    <li>
                        <a href="<?php bloginfo('url'); ?>">Home</a>
                    </li>
                    <li>
                        <span>会社概要</span>
                    </li>
                </ul>
            </div>
        </div>
        <div class="company-menu">
            <div class="cntr">
                <?php 
					wp_nav_menu(
						array (
							'theme_location' => 'third',
							'walker'         => new WPSE_78121_Sublevel_Walker
							)
						); 
				?>
            </div>
        </div>
        <div class="company-box">
            <div class="cntr">
                <div class="company-content">
                    <h2 class="title">
                        <em>OUTLINE</em>
                        <span>会社概要</span>
                    </h2>
                    <!-- <h3 class="sub-infos tc">
                        株式会社ビッグツリーテクノロジー&コンサルティング<br>
                        <span>BTC Corporation</span>
                    </h3> -->
                    <div class="dl-section">
                        <dl class="list is-dl">
                            <dt>
                                社名
                            </dt>
                            <dd>
                                株式会社ビッグツリーテクノロジー&コンサルティング<br>
                                （英文：BTC Corporation）
                            </dd>   
                        </dl>
                        <dl class="list is-dl">
                            <dt>
                                設立
                            </dt>
                            <dd>
                                2002年2月7日
                            </dd>   
                        </dl>
                        <dl class="list is-dl">
                            <dt>
                                資本金
                            </dt>
                            <dd>
                                1億円
                            </dd>   
                        </dl>
                        <dl class="list is-dl">
                            <dt>
                                代表者
                            </dt>
                            <dd>
                                代表取締役社長　杉山 健
                            </dd>   
                        </dl>
                        <dl class="list is-dl">
                            <dt>
                                従業員数
                            </dt>
                            <dd>
                                <span>215名</span> (連結・2019年2月1日時点)内、ベトナムオフィス19名
                            </dd>   
                        </dl>
                    </div>
                    <h2 class="title">
                        <em>SALES</em>
                        <span>売上推移</span>
                    </h2>
                    <div class="company-sales-img">
                        <img src="<?php echo get_template_directory_uri()?>/assets/img/img_company01.jpg" alt="" class="is-wide">
                    </div>
                    <h2 class="title">
                        <em>GLOBAL OFFICE</em>
                        <span>グローバルオフィス</span>
                    </h2>
                    <div class="company-global-office">
                        <div class="company-map v-pc">
                            <ul class="list-office">
                                <li class="tokyo">
                                    <div class="cont">
                                        <p class="area">東京</p>
                                        <!-- <p class="txt">全クライアントおよび全部署を<br>統括する本社</p> -->
                                        <p class="since">Since 2002.</p>
                                    </div>
                                </li>
                                <li class="sapporo">
                                    <div class="cont">
                                        <p class="area">札幌</p>
                                        <p class="txt">出張とリモートを組み合わせ、<br>上流工程からサービス可能なニアショアセンター</p>
                                        <p class="since">Since 2014.08</p>
                                    </div>
                                </li>
                                <li class="silicon-valley">
                                    <div class="cont">
                                        <p class="area">シリコンバレー</p>
                                        <p class="txt">世界最先端テクノロジーを発掘する<br>インキュベーション(投資)センター</p>
                                        <p class="since">Since 2017.06</p>
                                    </div>
                                </li>
                                <li class="vietnam">
                                    <div class="cont">
                                        <p class="area">ベトナム</p>
                                        <p class="txt">日本と同等の品質&スピードを<br>低コストで提供する<br>オフショアデリバリーセンター</p>
                                        <p class="since">Since 2017.02</p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <img src="<?php echo get_template_directory_uri()?>/assets/img/img_company02_2.jpg" alt="" class="is-wide globe-map">
                        <div class="company-global-img v-sp">
                            <img src="<?php echo get_template_directory_uri()?>/assets/img/img_company03.jpg" alt="" class="is-wide">
                            <div class="company-global-cont">
                                <ul>
                                    <li>
                                        <h4>東京</h4>
                                        <!-- <p>全クライアントおよび全部署を統括する本社</p> -->
                                        <p class="global-yer">Since 2002.</p>
                                    </li>
                                    <li>
                                        <h4>札幌</h4>
                                        <p>出張とリモートを組み合わせ、上流工程からサービス可能 なニアショアセンター</p>
                                        <p class="global-yer">Since 2014.08</p>
                                    </li>
                                    <li>
                                        <h4>シリコンバレー</h4>
                                        <p>世界最先端テクノロジーを発掘するインキュベーション(投資)センター</p>
                                        <p class="global-yer">Since 2017.06</p>
                                    </li>
                                    <li>
                                        <h4>ベトナム</h4>
                                        <p>日本と同等の品質&スピードを低コストで提供するオフショアデリバリーセンター</p>
                                        <p class="global-yer">Since 2017.02</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!-- <div class="cntr tr btn">
                            <a href="#" class="btn-readmore btn-address">
                                各事務所の住所はこちら
                            </a>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php get_footer(); ?>