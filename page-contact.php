<?php 
/**
 * The template for displaying sample page	
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package TEMPLATENAME
 */

get_header(); 

?>

    <section class="wrp sec-contact">
        <div class="page-title titlev3">
            <div class="cntr">
                <h2>
                    Contact <span>お問い合わせ</span>
                </h2>
            </div>
        </div>
        <div class="breadcrumbs">
            <div class="cntr">
                <ul>
                    <li>
                        <a href="<?php bloginfo('url'); ?>">Home</a>
                    </li>
                    <li>
                        <span>お問い合わせ</span>
                    </li>
                </ul>
            </div>
        </div>
        <div class="contact-box">
            <div class="cntr">
                <div class="contact-content">
                    <h2 class="title">
                        <em>CONTACT</em>
                        <span>お問い合わせ</span>
                    </h2>
                    <h3 class="sub-infos v2 tc">
                        下記フォームにご入力の上、「送信」ボタンをクリックしてください。<br>
                        お問い合わせにお答えするにはメールアドレスが必要です。お間違えのないようお願い致します。
                    </h3>
                    <?php echo do_shortcode('[contact-form-7 id="6110" title="Contact form 1"]'); ?>
                </div>
            </div>
        </div>
    </section>

<?php 

get_footer(); ?>