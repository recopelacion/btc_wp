<?php 

/**
 * The template for displaying sample page	
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package TEMPLATENAME
 */

get_header(); 

?>

    <section class="wrp sec-company">
        <div class="breadcrumbs">
            <div class="cntr">
                <ul>
                    <li>
                        <a href="<?php bloginfo('url'); ?>">Home</a>
                    </li>
                    <li>
                        <span>セキュリティポリシー</span>
                    </li>
                </ul>
            </div>
        </div>
        <div class="company-box">
            <div class="cntr">
                <div class="company-content policy">
                    <h2 class="title">
                        <em>SECURITY POLICY</em>
                        <span>セキュリティポリシー</span>
                    </h2>
                    <h4>基本方針</h4>
                    <p>株式会社ビッグツリーテクノロジー＆コンサルティングでは、「ITを利用し、文化を創造しつづける」というフィロソフィーに基づき、お客様のビジネスに貢献しつづけることを経営理念としております。 この理念を達成するために、強固な情報セキュリティーポリシーを策定し、維持、改善しつづけることが必要であると考えております。 当社では、情報資産に関わる全ての関係者を対象としてセキュリティに関する基本方針を示し、情報セキュリティの確保に努めて参ります。</p>
                    <h4>情報セキュリティマネジメントシステムの構築</h4>
                    <p>当社は、事業活動の円滑な遂行のため、情報セキュリティを管理していくための仕組みを構築します。 この仕組みは、業務の変更に伴って継続的に改善します。</p>
                    <h4>情報セキュリティ管理体制</h4>
                    <p>当社が保有する情報資産を管理していくために情報セキュリティの管理体制を整備します。 各部門に情報セキュリティ責任者を任命し、全社をスコープとしたセキュリティ管理体制を維持します。</p>
                    <h4>管理体制の維持・継続</h4>
                    <p>当社では、セキュリティ管理が確実に実施されていることを確認するために、内部監査体制を整備します。</p>
                    <h4>情報セキュリティリテラシーの向上</h4>
                    <p>
                        全社員・派遣社員に対し、セキュリティ教育・訓練を実施します。<br>
                        当社の事業活動における情報資産に関わる全員が、適切なセキュリティリテラシーを持ち、業務の遂行を円滑に行えるよう、教育・訓練を継続して実施します。
                    </p>
                    <h4>外部協力企業のセキュリティ管理体制の確認</h4>
                    <p>当社では、外部企業に業務を委託する場合があります。 委託する場合においても、情報資産の重要性については充分に認識していただくとともに、セキュリティ管理体制を確認し、適切なセキュリティレベルの維持を要請していきます。</p>
                </div>
            </div>
        </div>
    </section>

<?php get_footer(); ?>