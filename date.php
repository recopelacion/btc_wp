<?php 
/**
 * The template for displaying date	archive
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package btc
 */

get_header(); 

?>

    
<section class="wrp sec-blog">
        <div class="page-title titlev2">
            <div class="cntr">
                <h2>
                    News Archive <span>ブログ</span>
                </h2>
            </div>
        </div>
        <div class="breadcrumbs">
            <div class="cntr">
                <ul>
                    <li>
                        <a href="<?php bloginfo('url'); ?>">Home</a>
                    </li>
                    <li>
                        <span>News Archive</span>
                    </li>
                </ul>
            </div>
        </div>
        <div class="blog-box">
            <div class="cntr">
                <div class="blog-wrap">
                    <h2 class="title">
                        <em>News Archive</em>
                        <span><?php echo get_the_date('Y'); ?></span>
                    </h2>
                    <div class="blog-cont">
                        <div class="gap gap-30 gap-0-xs">
                            <div class="md-8 xs-12">
                                <div class="gap gap-10 gap-0-xs">
                                    <?php while (have_posts()) : the_post(); ?>
                                    
                                    <div id="post-<?php the_ID(); ?>" <?php post_class('md-6 xs-12 item'); ?>>
                                        <a href="<?php the_permalink(); ?>">
                                            <div class="card card-blog">
                                                <div class="card-header blog-header">
                                                     <?php
                                                        $thumb_id = get_post_thumbnail_id(get_the_ID());
                                                        $alt = get_post_meta($thumb_id, '_wp_attachment_image_alt', true);
                                                    ?>
                                                    <img title="<?php the_title(); ?>" alt="<?php echo $alt; ?>" class="wp-post-image is-wide" src="<?=wp_get_attachment_url( get_post_thumbnail_id() ); ?>">
                                                </div>
                                                <div class="card-body blog-body">
                                                    <div class="card-date blog-date">
                                                        <h4><?php the_time('Y.m.d'); ?> <span class="category"><?php $cat = get_the_category(); echo $cat[0]->cat_name; ?></span></h4>
                                                    </div>
                                                    <div class="card-title blog-title">
                                                        <h3><?php the_title(); ?></h3>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>

                                    <?php endwhile;
                                        wp_reset_query();
                                    ?>
                                </div>
                                <?php wp_pagination(); ?>
                            </div>
                            <div class="md-4 xs-12">
                                <?php get_sidebar(); ?>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </section>

<?php get_footer(); ?>