<?php

 /*
 * ----------------------------------------------------------------------------------------
 *  ENQUEU THE STYLE
 * ----------------------------------------------------------------------------------------
 */
function zeroten_styles(){
    wp_enqueue_style( 'slick', get_template_directory_uri() . '/assets/css/slick.css' );
    wp_enqueue_style( 'slick-theme', get_template_directory_uri() . '/assets/css/slick-theme.css' );
    wp_enqueue_style( 'main', get_template_directory_uri() . '/assets/css/main.css' );
    // wp_enqueue_style( 'quicksand', 'https://fonts.google.com/specimen/Quicksand' );
    wp_enqueue_style( 'font-all', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.min.css' );
    wp_enqueue_style( 'font-brand', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/brands.min.css' );
    wp_enqueue_style( 'font-awesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/fontawesome.min.css' );
    // wp_enqueue_style( 'db-font', 'https://db.onlinewebfonts.com/c/3b2646a48566403a55f62ceddbecbe18?family=Hiragino+Sans+GBd' );
    wp_enqueue_style( 'owl', get_template_directory_uri() . '/assets/css/owl/owl.carousel.min.css' );
    wp_enqueue_style( 'animate', get_template_directory_uri() . '/assets/css/owl/animate.css' );
    wp_enqueue_style( 'aos', 'https://unpkg.com/aos@2.3.1/dist/aos.css' );
	wp_enqueue_style( 'style', get_stylesheet_uri() );
}
add_action ('wp_enqueue_scripts', 'zeroten_styles');


/*
 * ----------------------------------------------------------------------------------------
 *  ENQUEU THE SCRIPT
 * ----------------------------------------------------------------------------------------
 */
function zeroten_scripts(){
    wp_enqueue_script( 'aos', 'https://unpkg.com/aos@2.3.1/dist/aos.js', array( 'jquery' ), true, true );
    wp_enqueue_script( 'slick-js', get_template_directory_uri() . '/assets/js/slick.min.js', array( 'jquery' ), true, true );
	wp_enqueue_script( 'main', get_template_directory_uri() . '/assets/js/main.js', array( 'jquery' ), true, true );
}
add_action ('wp_enqueue_scripts', 'zeroten_scripts');