<?php

/*
 * ----------------------------------------------------------------------------------------
 *  THEME SUPPORT
 * ----------------------------------------------------------------------------------------
 */
add_theme_support( 'post-thumbnails' );
add_theme_support( 'post-formats', array( 'aside', 'gallery', 'link', 'image', 'qoute', 'status', 'video', 'audio', 'chat' ) );


/*
 * ----------------------------------------------------------------------------------------
 *  REGISTER THE NAV MENU
 * ----------------------------------------------------------------------------------------
 */
function nav_theme_setup() {
    add_theme_support('menus');

    register_nav_menu('primary', 'Header Navigation');
    register_nav_menu('secondary', 'Footer Navigation');
    register_nav_menu('third', 'Company Navigation');
    register_nav_menu('fourth', 'Blog Navigation');
}
add_action('init', 'nav_theme_setup');

/*
 * ----------------------------------------------------------------------------------------
 *  ADD HTML SUBMENU
 * ----------------------------------------------------------------------------------------
 */
class WPSE_78121_Sublevel_Walker extends Walker_Nav_Menu
{
    function start_lvl( &$output, $depth = 0, $args = array() ) {
        $indent = str_repeat("\t", $depth);
        $output .= "\n$indent<div class='sub-menu'><ul>\n";
    }
    function end_lvl( &$output, $depth = 0, $args = array() ) {
        $indent = str_repeat("\t", $depth);
        $output .= "$indent</ul></div>\n";
    }
}

add_filter( 'nav_menu_css_class', function($classes) {
    $classes[] = 'nav-item';
    return $classes;
}, 10, 1 );

/*
 * ----------------------------------------------------------------------------------------
 *  RENAME THE POSTS TO NEWS
 * ----------------------------------------------------------------------------------------
 */
function revcon_change_post_label() {
    global $menu;
    global $submenu;
    $menu[5][0] = 'News';
    $submenu['edit.php'][5][0] = 'News';
    $submenu['edit.php'][10][0] = 'Add News';
    $submenu['edit.php'][16][0] = 'News Tags';
}
function revcon_change_post_object() {
    global $wp_post_types;
    $labels = &$wp_post_types['post']->labels;
    $labels->name = 'News';
    $labels->singular_name = 'News';
    $labels->add_new = 'Add News';
    $labels->add_new_item = 'Add News';
    $labels->edit_item = 'Edit News';
    $labels->new_item = 'News';
    $labels->view_item = 'View News';
    $labels->search_items = 'Search News';
    $labels->not_found = 'No News found';
    $labels->not_found_in_trash = 'No News found in Trash';
    $labels->all_items = 'All News';
    $labels->menu_name = 'News';
    $labels->name_admin_bar = 'News';
}
 
add_action( 'admin_menu', 'revcon_change_post_label' );
add_action( 'init', 'revcon_change_post_object' );

/*
 * ----------------------------------------------------------------------------------------
 *  RSS FEED
 * ----------------------------------------------------------------------------------------
 */

function add_featured_image_to_feed($content) {
global $post;
if ( has_post_thumbnail( $post->ID ) ){
$content = '' . get_the_post_thumbnail( $post->ID ) . '' . $content;
}
return $content;
}

add_filter('the_excerpt_rss', 'add_featured_image_to_feed', 1000, 1);
add_filter('the_content_feed', 'add_featured_image_to_feed', 1000, 1);

/*
 * ----------------------------------------------------------------------------------------
 *  SEARCH AJAX
 * ----------------------------------------------------------------------------------------
 */

add_action( 'wp_footer', 'ajax_fetch' );
function ajax_fetch() {
?>
<script type="text/javascript">
function fetch(){

    jQuery.ajax({
        url: '<?php echo admin_url('admin-ajax.php'); ?>',
        type: 'post',
        data: { action: 'data_fetch', keyword: jQuery('#keyword').val() },
        success: function(data) {
            jQuery('#datafetch').html( data );
        }
    });

}
</script>

<?php
}

// the ajax function
add_action('wp_ajax_data_fetch' , 'data_fetch');
add_action('wp_ajax_nopriv_data_fetch','data_fetch');
function data_fetch(){

    $the_query = new WP_Query( array( 'posts_per_page' => -1, 's' => esc_attr( $_POST['keyword'] ), 'post_type' => 'post' ) );
    if( $the_query->have_posts() ) :
        while( $the_query->have_posts() ): $the_query->the_post(); ?>

            <h2><a href="<?php echo esc_url( post_permalink() ); ?>"><?php the_title();?></a></h2>

        <?php endwhile; else: ?>
            <h2>No results found</h2>
        <?php
        wp_reset_postdata();  
    endif;

    die();
}

// Change Name Featured Image
add_action( 'admin_head', 'change_meta_box_title' );
function change_meta_box_title() {
    remove_meta_box( 'postimagediv', 'slider', 'side' ); //replace post_type from your post type name
    add_meta_box('postimagediv', __('PCスライダー画像 ※約2775px x 約1340px（推奨サイズ）'), 'post_thumbnail_meta_box', 'slider', 'side', 'high');
}


//Get the Popular Posts
function wpb_set_post_views($postID) {
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}
//To keep the count accurate, lets get rid of prefetching
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);

function wpb_track_post_views ($post_id) {
    if ( !is_single() ) return;
    if ( empty ( $post_id) ) {
        global $post;
        $post_id = $post->ID;    
    }
    wpb_set_post_views($post_id);
}
add_action( 'wp_head', 'wpb_track_post_views');

function wpb_get_post_views($postID){
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0 View";
    }
    return $count.' Views';
}


/**
 * Social media share buttons
 */
function wcr_share_buttons() {
    $url = urlencode(get_the_permalink());
    $title = urlencode(html_entity_decode(get_the_title(), ENT_COMPAT, 'UTF-8'));
    $media = urlencode(get_the_post_thumbnail_url(get_the_ID(), 'full'));

    include( locate_template('inc/share-template.php', false, false) );
}

// function sydney_include_projects_archive($query) {
// 	if ( !is_admin() && $query->is_main_query() ) {
// 	    if ( $query->is_archive() ) {
// 			$query->set('post_type', array( 'post', 'core-technology' ) );
// 		}
// 	}
// }
// add_action('pre_get_posts','sydney_include_projects_archive');

// Archive to Date
function wpa_date_posts_per_page( $query ) {
    if ( !is_admin()
        && $query->is_date()
        && $query->is_main_query() ) {
            $query->set( 'post_type', array( 'post' ), 'posts_per_page', 10 );
    }
}
add_action( 'pre_get_posts', 'wpa_date_posts_per_page' );

//Numeric Pagination
function wp_pagination() {
    global $wp_query;
    $big = 12345678;
    $page_format = paginate_links( array(
        'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
        'format' => '?paged=%#%',
        'current' => max( 1, get_query_var('paged') ),
        'total' => $wp_query->max_num_pages,
        'type'  => 'array',
        'prev_text' => __('<'),
        'next_text' => __('>'),
    ) );
    if( is_array($page_format) ) {
                $paged = ( get_query_var('paged') == 0 ) ? 1 : get_query_var('paged');
                echo '<div class="pagination"><ul>';
                foreach ( $page_format as $page ) {
                        echo "<li>$page</li>";
                }
                echo '</ul></div>';
    }
}



//Custom Taxonomy Category
function taxonomy_slug_rewrite($wp_rewrite) {
    $rules = array();
    // get all custom taxonomies
    $taxonomies = get_taxonomies(array('_builtin' => false), 'objects');
    // get all custom post types
    $post_types = get_post_types(array('public' => true, '_builtin' => false), 'objects');

    foreach ($post_types as $post_type) {
        foreach ($taxonomies as $taxonomy) {

            // go through all post types which this taxonomy is assigned to
            foreach ($taxonomy->object_type as $object_type) {

                // check if taxonomy is registered for this custom type
                if ($object_type == $post_type->rewrite['slug']) {

                    // get category objects
                    $terms = get_categories(array('type' => $object_type, 'taxonomy' => $taxonomy->name, 'hide_empty' => 0));

                    // make rules
                    foreach ($terms as $term) {
                        $rules[$object_type . '/' . $term->slug . '/?$'] = 'index.php?' . $term->taxonomy . '=' . $term->slug;
                    }
                }
            }
        }
    }
    // merge with global rules
    $wp_rewrite->rules = $rules + $wp_rewrite->rules;
}
add_filter('generate_rewrite_rules', 'taxonomy_slug_rewrite');
