<?php

function core_technology_custom_posts() {
	$labels = array(
		'name'                => __( 'Core Technology' ),
		'singular_name'       => __( 'Core Technology'),
		'menu_name'           => __( 'Core Technology'),
		'parent_item_colon'   => __( 'Parent Core Technology'),
		'all_items'           => __( 'All Core Technology'),
		'view_item'           => __( 'View Core Technology'),
		'add_new_item'        => __( 'Add New Core Technology'),
		'add_new'             => __( 'Add New'),
		'edit_item'           => __( 'Edit Core Technology'),
		'update_item'         => __( 'Update Core Technology'),
		'search_items'        => __( 'Search Core Technology'),
		'not_found'           => __( 'Not Found'),
		'not_found_in_trash'  => __( 'Not found in Trash')
	);
	$args = array(
		'label'               => __( 'core-technology'),
		'description'         => __( 'Best Core Technology'),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'revisions', 'custom-fields'),
		'public'              => true,
		'hierarchical'        => false,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_icon' 		=> 'dashicons-networking',
		'has_archive'         => true,
		'can_export'          => true,
		'exclude_from_search' => false,
	        'yarpp_support'       => true,
		'taxonomies' 	      => array('core-technology_tag', 'core-technology_cat'),
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
		'rewrite' => array(
			'slug' => 'core-technology',
			'with_front' => false
		)
);
	register_post_type( 'core-technology', $args );
}
add_action( 'init', 'core_technology_custom_posts', 0 );

function slider_custom_posts() {
	$labels = array(
		'name'                => __( 'Sliders' ),
		'singular_name'       => __( 'Slider'),
		'menu_name'           => __( 'スライダー画像'),
		'parent_item_colon'   => __( 'Parent Slider'),
		'all_items'           => __( 'All Slider'),
		'view_item'           => __( 'View Slider'),
		'add_new_item'        => __( 'Add New Slider'),
		'add_new'             => __( 'Add New'),
		'edit_item'           => __( 'Edit Slider'),
		'update_item'         => __( 'Update Slider'),
		'search_items'        => __( 'Search Slider'),
		'not_found'           => __( 'Not Found'),
		'not_found_in_trash'  => __( 'Not found in Trash')
	);
	$args = array(
		'label'               => __( 'slider'),
		'description'         => __( 'Best Sliders'),
		'labels'              => $labels,
		'supports'            => array( 'title', 'thumbnail', 'author'),
		'public'              => true,
		'hierarchical'        => false,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_icon' 		=> 'dashicons-format-image',
		'has_archive'         => true,
		'can_export'          => true,
		'exclude_from_search' => false,
	        'yarpp_support'       => true,
		'taxonomies' 	      => array('slider_tag', 'slider_cat'),
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
		'rewrite' => array(
			'slug' => 'slider',
			'with_front' => false
		)
);
	register_post_type( 'slider', $args );
}
add_action( 'init', 'slider_custom_posts', 0 );

function member_custom_posts() {
	$labels = array(
		'name'                => __( 'Members' ),
		'singular_name'       => __( 'Member'),
		'menu_name'           => __( '役員紹介'),
		'parent_item_colon'   => __( 'Parent Member'),
		'all_items'           => __( 'All Member'),
		'view_item'           => __( 'View Member'),
		'add_new_item'        => __( 'Add New Member'),
		'add_new'             => __( 'Add New'),
		'edit_item'           => __( 'Edit Member'),
		'update_item'         => __( 'Update Member'),
		'search_items'        => __( 'Search Member'),
		'not_found'           => __( 'Not Found'),
		'not_found_in_trash'  => __( 'Not found in Trash')
	);
	$args = array(
		'label'               => __( 'member'),
		'description'         => __( 'Best Members'),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'revisions', 'custom-fields'),
		'public'              => true,
		'hierarchical'        => false,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_icon' 		=> 'dashicons-groups',
		'has_archive'         => true,
		'can_export'          => true,
		'exclude_from_search' => false,
	        'yarpp_support'       => true,
		'taxonomies' 	      => array('member_tag', 'member_cat'),
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
		'rewrite' => array(
			'slug' => 'member',
			'with_front' => false
		)
);
	register_post_type( 'member', $args );
}
add_action( 'init', 'member_custom_posts', 0 );