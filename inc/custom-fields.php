<?php

// members custom fields

function member_custom_fields() {

	add_meta_box(
		'member_meta',
		__( "Member's Informations" ),
		'member_metabox_callback',
		'member',
		'normal',
		'high'
	);
}

add_action( 'add_meta_boxes', 'member_custom_fields' );

// callback
function member_metabox_callback( $post ) {
	wp_nonce_field( basename( __FILE__ ), 'member_once' );
	$member_stored_meta = get_post_meta( $post->ID );

	// member's name
	$jp_name = (!empty($member_stored_meta['jp_name']) ? esc_attr(  $member_stored_meta['jp_name'][0] ) : null);

    ?>

	<style>
		.member_input_lists {
			max-width: 500px;
			width: 100%;
		}
    	.member_input_lists ul li, .cols2, .cols3 {
			display: -webkit-box;
			display: -webkit-flex;
			display: -moz-box;
			display: -ms-flexbox;
			display: flex;
			-webkit-flex-wrap: wrap;
				-ms-flex-wrap: wrap;
					flex-wrap: wrap;
			position: relative;
			margin-bottom: 20px;
			width: 100%;
		}
		.member_input_lists ul li label {
			flex: 0 0 20%;
			max-width: 20%;
			padding-right: 30px;
			font-size: 14px;
			font-weight: 600;
		}
		.member_input_lists ul li.has-2cols, .cols2, .cols3 {
			-webkit-box-align: center;
			-webkit-align-items: center;
				-moz-box-align: center;
				-ms-flex-align: center;
					align-items: center;
		}
		.member_input_lists ul li.has-2cols .cols2 strong {
			display: inline-block;
			min-width: 50px;
		}
		.member_input_lists ul li.has-2cols .cols2, .cols3 {
			margin-bottom: 10px;
		}
		.member_input_lists ul li.has-2cols .cols2:last-child, .cols3:last-child {
			margin-bottom: 0;
		}
		.member_input_lists ul li textarea {
			min-height: 100px;
			max-height: 150px;
			min-width: 100%;
			max-width: 100%;
		}
		.input-area {
			flex: auto;
		}
		
		.member_input_lists ul li.has-2cols .cols2 input {
			flex: auto;
		}
		.member_input_lists ul li .input_area {
			flex: auto;
		}
		.member_input_lists ul li .input_area input {
			width: 100%;
		}
		.cols3 .input_area {
			margin-left: 10px;
		}
    </style>

	<div class="member_input_lists">
		<ul>
			<li class="has-2cols">
				<label for="Name">Name: </label>
				<div class="dflex-ai input-area">
					<div class="cols2">
						<strong> (JP) :</strong>
						<input type="text" name="jp_name" value="<?php echo $jp_name; ?>">
					</div>
				</div>
		</ul>
	</div>


    <?php
}

function member_meta_save( $post_id) {

	$is_autosave = wp_is_post_autosave( $post_id );
    $is_revision = wp_is_post_revision( $post_id );
	
	// member's name
	$jp_name = ( isset( $_POST[ 'jp_name' ] ) && wp_verify_nonce( $_POST[ 'jp_name' ], basename( __FILE__ ) ) ) ? 'true' : 'false';

    if ($is_autosave || $is_revision || !$jp_name) {
        return;
    }

	if ( isset( $_POST[ 'jp_name' ] ) 
		) {
		update_post_meta( $post_id, 'jp_name', $_POST[ 'jp_name' ] );
    }
    
}
add_action( 'save_post', 'member_meta_save' );



// Slider

function slider_custom_fields() {

	add_meta_box(
		'slider_meta',
		__( "SPスライダー画像 ※約775px x 約437px（推奨サイズ）" ),
		'slider_metabox_callback',
		'slider',
		'normal',
		'high'
	);
}

add_action( 'add_meta_boxes', 'slider_custom_fields' );

// callback
function slider_metabox_callback( $post ) {
	wp_nonce_field( basename( __FILE__ ), 'slider_once' );
	$slider_stored_meta = get_post_meta( $post->ID );

	
	$slider_img = (!empty($slider_stored_meta['slider_img']) ? esc_attr(  $slider_stored_meta['slider_img'][0] ) : null);

    ?>

	<style>
		.member_input_lists {
			max-width: 500px;
			width: 100%;
		}
    	.member_input_lists ul li, .cols2, .cols3 {
			display: -webkit-box;
			display: -webkit-flex;
			display: -moz-box;
			display: -ms-flexbox;
			display: flex;
			-webkit-flex-wrap: wrap;
				-ms-flex-wrap: wrap;
					flex-wrap: wrap;
			position: relative;
			margin-bottom: 20px;
			width: 100%;
		}
		.member_input_lists ul li label {
			flex: 0 0 20%;
			max-width: 20%;
			padding-right: 30px;
			font-size: 14px;
			font-weight: 600;
		}
		.member_input_lists ul li.has-2cols, .cols2, .cols3 {
			-webkit-box-align: center;
			-webkit-align-items: center;
				-moz-box-align: center;
				-ms-flex-align: center;
					align-items: center;
		}
		.member_input_lists ul li.has-2cols .cols2 strong {
			display: inline-block;
			min-width: 50px;
		}
		.member_input_lists ul li.has-2cols .cols2, .cols3 {
			margin-bottom: 10px;
		}
		.member_input_lists ul li.has-2cols .cols2:last-child, .cols3:last-child {
			margin-bottom: 0;
		}
		.member_input_lists ul li textarea {
			min-height: 100px;
			max-height: 150px;
			min-width: 100%;
			max-width: 100%;
		}
		.input-area {
			flex: auto;
		}
		
		.member_input_lists ul li.has-2cols .cols2 input {
			flex: auto;
		}
		.member_input_lists ul li .input_area {
			flex: auto;
		}
		.member_input_lists ul li .input_area input {
			width: 100%;
		}
		.cols3 .input_area {
			margin-left: 10px;
		}
    </style>

	<div class="member_input_lists">
		<ul>
			<li class="has-2cols">
				<label for="Name">Slider Image</label>
				<div class="dflex-ai input-area">
					<div class="cols2">
						<input type="text" name="slider_img" id="slider_img" class="meta-image regular-text" value="<?php echo $slider_img; ?>">
						<input type="button" class="button image-upload" value="Browse">
					</div>
				</div>
				<div class="image-preview"><img src="<?php echo esc_url($slider_img); ?>" style="max-width: 250px;"></div>
		</ul>
	</div>

	<script>
  jQuery(document).ready(function($) {
    // Instantiates the variable that holds the media library frame.
    var meta_image_frame
    // Runs when the image button is clicked.
    $('.image-upload').click(function(e) {
      // Get preview pane
      var meta_image_preview = $(this)
        .parent()
        .parent()
        .children('.image-preview')
      // Prevents the default action from occuring.
      e.preventDefault()
      var meta_image = $(this)
        .parent()
        .children('.meta-image')
      // If the frame already exists, re-open it.
      if (meta_image_frame) {
        meta_image_frame.open()
        return
      }
      // Sets up the media library frame
      meta_image_frame = wp.media.frames.meta_image_frame = wp.media({
        title: meta_image.title,
        button: {
          text: meta_image.button,
        },
      })
      // Runs when an image is selected.
      meta_image_frame.on('select', function() {
        // Grabs the attachment selection and creates a JSON representation of the model.
        var media_attachment = meta_image_frame
          .state()
          .get('selection')
          .first()
          .toJSON()
        // Sends the attachment URL to our custom image input field.
        meta_image.val(media_attachment.url)
        meta_image_preview.children('img').attr('src', media_attachment.url)
      })
      // Opens the media library frame.
      meta_image_frame.open()
    })
  })
</script>


    <?php
}

function slider_meta_save( $post_id) {

	$is_autosave = wp_is_post_autosave( $post_id );
    $is_revision = wp_is_post_revision( $post_id );
	
	// member's name
	$slider_img = ( isset( $_POST[ 'slider_img' ] ) && wp_verify_nonce( $_POST[ 'slider_img' ], basename( __FILE__ ) ) ) ? 'true' : 'false';

    if ($is_autosave || $is_revision || !$slider_img) {
        return;
    }

	if ( isset( $_POST[ 'slider_img' ] ) 
		) {
		update_post_meta( $post_id, 'slider_img', $_POST[ 'slider_img' ] );
    }
    
}
add_action( 'save_post', 'slider_meta_save' );




