<ul class="social-media-list">
    <li class="hatena">
        <a class="share-hatena" href="https://b.hatena.ne.jp/add?mode=confirm&amp;url=<?php echo $url; ?>" target="_blank">
            <img src="<?php echo get_template_directory_uri()?>/assets/img/ico_bt_hatena.png" alt="" class="is-wide">
        </a>
    </li>
    <li class="fb">
        <a class="share-facebook" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $url; ?>" target="_blank">
            <i class="fab fa-facebook-f"></i>
        </a>
    </li>
    <li class="pin">
        <a class="share-pinterest" href="http://pinterest.com/pin/create/button/?url=<?php echo $url; ?>&amp;media=<?php echo $media;   ?>&amp;description=<?php echo $title; ?>" target="_blank">
            <i class="fab fa-pinterest"></i>
        </a>
    </li>
    <li class="twit">
        <a class="share-twitter" href="https://twitter.com/intent/tweet?text=<?php echo $title; ?>&amp;url=<?php echo $url; ?>" target="_blank">
            <i class="fab fa-twitter"></i>
        </a>
    </li>
    <li class="pocket">
        <a class="share-pocket" href="https://getpocket.com/edit?url=<?php echo $url; ?>" target="_blank">
            <i class="fab fa-get-pocket"></i>
        </a>
    </li>
    <li class="line">
        <a class="share-line" href="https://social-plugins.line.me/lineit/share?url=<?php echo $url; ?>" target="_blank">
            <i class="fab fa-line"></i>
        </a>
    </li>
</ul>