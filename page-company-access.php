<?php
	/***
		Template Name: Company Access
	***/
?>
<?php get_header(); ?>

    <section class="wrp sec-company">
        <div class="page-title titlev1">
            <div class="cntr">
                <h2>
                    Company <span>アクセス</span>
                </h2>
            </div>
        </div>
        <div class="breadcrumbs">
            <div class="cntr">
                <ul>
                    <li>
                        <a href="<?php bloginfo('url'); ?>">Home</a>
                    </li>
                    <li>
                        <a href="<?php bloginfo('url'); ?>/company">会社概要</a>
                    </li>
                    <li>
                        <span>アクセス</span>
                    </li>
                </ul>
            </div>
        </div>
        <div class="company-menu">
            <div class="cntr">
                <?php 
					wp_nav_menu(
						array (
							'theme_location' => 'third',
							'walker'         => new WPSE_78121_Sublevel_Walker
							)
						); 
				?>
            </div>
        </div>
        <div class="company-box">
            <div class="cntr">
                <div class="company-content">
                    <h2 class="title">
                        <em>TOKYO OFFICE</em>
                        <span>東京オフィス（本社）</span>
                    </h2>
                    <div class="gap gap-35 gap-15-xs company-access-box">
                        <div class="md-6 xs-12">
                            <div class="company-access-cont">
                                <p>
                                    〒106-0041<br>
                                    東京都港区麻布台2丁目3番5号　ノアビル3階<br>
                                    TEL： 03-5114-6171（代表）<br>
                                    FAX： 03-5114-6172<br><br>
                                    <!-- 東京メトロ日比谷線「神谷町駅」徒歩5分<br>
                                    都営大江戸線「赤羽橋駅」徒歩7分<br>
                                    東京メトロ南北線「麻布十番駅」徒歩10分<br>
                                    都営大江戸線「麻布十番駅」徒歩10分 -->
                                </p>
                                <div class="btn">
                                    <a href="https://goo.gl/maps/7u7rGLGKkPJHeqkW7" class="btn-readmore btn-address" target="_blank">
                                        大きな地図で確認する
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="md-6 xs-12">
                            <div class="access-img">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3241.718320284335!2d139.742531!3d35.659311!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x60188b77435772eb%3A0xeafd0ac53e0c4906!2z5qCq5byP5Lya56S-44OT44OD44Kw44OE44Oq44O844OG44Kv44OO44Ot44K444O877yG44Kz44Oz44K144Or44OG44Kj44Oz44Kw!5e0!3m2!1sja!2sph!4v1561688429956!5m2!1sja!2sph" width="100%" height="270" frameborder="0" style="border:0" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                    <h2 class="title">
                        <!-- <em>TOKYO OFFICE</em> -->
                        <span>東京・東麻布オフィス</span>
                    </h2>
                    <div class="gap gap-35 gap-15-xs company-access-box">
                        <div class="md-6 xs-12">
                            <div class="company-access-cont">
                                <p>
                                    〒106-0044<br> 
                                    東京都港区東麻布1-9-15東麻布一丁目ビル5F
                                </p>
                                <div class="btn">
                                    <a href="https://goo.gl/maps/RMCUEYKoDkjxu8EG6" class="btn-readmore btn-address" target="_blank">
                                        大きな地図で確認する
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="md-6 xs-12">
                            <div class="access-img">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3241.785705037279!2d139.7419780156824!3d35.657651038871336!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x60188bbd0d135db3%3A0x894b5a361df5f805!2z5pel5pys44CB44CSMTA2LTAwNDQg5p2x5Lqs6YO95riv5Yy65p2x6bq75biD77yR5LiB55uu77yZ4oiS77yR77yVIOadsem6u-W4gyAx5LiB55uu44OT44OrNUY!5e0!3m2!1sja!2sph!4v1561689116581!5m2!1sja!2sph" width="100%" height="270" frameborder="0" style="border:0" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                    <h2 class="title">
                        <em>SAPPORO OFFICE</em>
                        <span>札幌オフィス</span>
                    </h2>
                    <div class="gap gap-35 gap-15-xs company-access-box">
                        <div class="md-6 xs-12">
                            <div class="company-access-cont">
                                <p>
                                    〒060-0001<br>
                                    北海道札幌市中央区北1条西3-3 敷島プラザビル4F<br>
                                    TEL： 011-206-1914<br><br>
                                    大通駅 徒歩3分<br>
                                    札幌駅 徒歩7分
                                </p>
                                <div class="btn">
                                    <a href="https://goo.gl/maps/YULMzoy26xE2" class="btn-readmore btn-address" target="_blank">
                                        大きな地図で確認する
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="md-6 xs-12">
                            <div class="access-img">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2915.001096205747!2d141.35001211582758!3d43.06244449844631!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x5f0b299dbea7f851%3A0x48bb3327ce53d03c!2z5pW35bO244OX44Op44K244OT44Or!5e0!3m2!1sja!2sph!4v1561628874289!5m2!1sja!2sph" width="100%" height="270" frameborder="0" style="border:0" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                    <h2 class="title">
                        <em>VIETNAM OFFICE</em>
                        <span>ベトナムオフィス</span>
                    </h2>
                    <div class="gap gap-35 gap-15-xs company-access-box">
                        <div class="md-6 xs-12">
                            <div class="company-access-cont">
                                <p>
                                    15F Ladeco Building, No.266, Doi Can Street,
                                    Ba Dinh District, Hanoi Vietnam<br>
                                    TEL：+84-24-7300-3848<br>
                                    （日本からは050-5318-9954）<br><br>
                                    ハノイ ノイバイ国際空港よりタクシー30分
                                </p>
                                <div class="btn">
                                    <a href="https://goo.gl/maps/k1o9DBgL2opuhuzA9" class="btn-readmore btn-address" target="_blank">
                                        大きな地図で確認する
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="md-6 xs-12">
                            <div class="access-img">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3723.912005479164!2d105.8136897!3d21.0362066!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xa47a2ab901e38e7e!2sBigtree+Technology+%26+Consulting+Vietnam+Co.%2C+Ltd.!5e0!3m2!1sja!2sph!4v1561688522534!5m2!1sja!2sph" width="100%" height="270" frameborder="0" style="border:0" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                    <h2 class="title">
                        <em>SILICON VALLEY OFFICE</em>
                        <span>シリコンバレーオフィス</span>
                    </h2>
                    <div class="gap gap-35 gap-15-xs company-access-box">
                        <div class="md-6 xs-12">
                            <div class="company-access-cont">
                                <p>
                                    3350 Scott Blvd. #29 Santa Clara, CA 95054<br>
                                    TEL：+1-408-252-6200
                                </p>
                                <div class="btn">
                                    <a href="https://goo.gl/maps/2NmZWbKJojz" class="btn-readmore btn-address" target="_blank">
                                        大きな地図で確認する
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="md-6 xs-12">
                            <div class="access-img">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d25363.488742290065!2d-121.9931451!3d37.3795179!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x808fc9e4dcbcbdb9%3A0x1bd4a27a0ae43f97!2zMzM1MCBTY290dCBCbHZkICMyOSwgU2FudGEgQ2xhcmEsIENBIDk1MDUxIOOCouODoeODquOCq-WQiOihhuWbvQ!5e0!3m2!1sja!2sph!4v1561629063602!5m2!1sja!2sph" width="100%" height="270" frameborder="0" style="border:0" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php get_footer(); ?>