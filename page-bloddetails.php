<?php
	/***
		Template Name: Blog Details
	***/
?>
<?php get_header(); ?>

    <section class="wrp sec-blog">
        <div class="breadcrumbs">
            <div class="cntr">
                <ul>
                    <li>
                        <a href="<?php bloginfo('url'); ?>">Home</a>
                    </li>
                    <li>
                        <a href="<?php bloginfo('url'); ?>/blog">ブログ</a>
                    </li>
                    <li>
                        <span>「第13回 CTC SaaS Day」について RPA（S/Wロボット）</span>
                    </li>
                </ul>
            </div>
        </div>
        <div class="blog-details">
            <div class="cntr">
                <div class="gap gap-30 gap-0-xs">
                    <div class="md-8 xs-12">
                        <div class="blog-detail-box">
                            <div class="blog-detail-header">
                                <h2 class="blog-detail-title">株式会社メンバーズ社内向けセミナー『基礎からのRPA講座～導入プロジェクトの進め方を具体事例で学ぶ～』</h2>
                                <div class="blog-detail-date-cat">
                                    <h4>2018.05.24 <span>Cloud</span></h4>
                                </div>
                                <div class="social-media-icon">
                                    <img src="<?php echo get_template_directory_uri()?>/assets/img/ico_social.jpg" alt="">
                                </div>
                            </div>
                            <div class="blog-detail-body">
                                <div class="blog-detail-img">
                                    <img src="<?php echo get_template_directory_uri()?>/assets/img/img_blogdetail01.jpg" class="is-wide" alt="">
                                </div>
                                <div class="blog-detail-cont">
                                    <p>
                                        2017年11月9日、CTC様主催の「第13回CTC SaaS Day」セミナーにSI事業部 取締役の水本が登壇しました。<br>
                                        弊社水本は『RPA導入プロジェクトの進め方』と題し、プロジェクト計画の立て方や効果測定
                                        の方法など、RPA導入プロジェクトを進めるにあたっての検討すべきポイントを、実際のユー
                                        ザー事例を交えてご紹介。
                                    </p>
                                    <h3>『基礎からのRPA講座～導入プロジェクトの』</h3>
                                    <p>
                                        2017年11月9日、CTC様主催の「第13回CTC SaaS Day」セミナーにSI事業部 取締役の水本が登壇しました。<br>
                                        弊社水本は『RPA導入プロジェクトの進め方』と題し、プロジェクト計画の立て方や効果測定の方法など、RPA導入プロジェクトを進めるにあたっての検討すべきポイントを、実際のユーザー事例を交えてご紹介。
                                    </p>
                                    <ul>
                                        <li>・WEBサイトやWEBサービスの開発に特化している</li>
                                        <li>・WEBサイトやWEBサービスの開発に特化している</li>
                                        <li>・WEBサイトやWEBサービスの開発に特化している</li>
                                    </ul>
                                    <p>
                                        2017年11月9日、CTC様主催の「第13回CTC SaaS Day」セミナーにSI事業部 取締役の水本が登壇しました。<br>
                                        弊社水本は『RPA導入プロジェクトの進め方』と題し、プロジェクト計画の立て方や効果測定の方法など、RPA導入プロジェクトを進めるにあたっての検討すべきポイントを、実際のユーザー事例を交えてご紹介。
                                    </p>
                                    <div class="social-media-icon">
                                        <img src="<?php echo get_template_directory_uri()?>/assets/img/ico_social.jpg" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="blog-detail-footer">
                                <ul class="blog-related-list">
                                    <li>
                                        <a href="#" class="related-item">
                                            <div class="related-img">
                                                <img src="<?php echo get_template_directory_uri()?>/assets/img/img_blogdetail01.jpg" alt="" class="is-wide">
                                            </div>
                                            <div class="related-cont">
                                                <h3>「第13回 CTC SaaS Day」について RPA（S/Wロボット）の活用により実現する働き方改革 ～</h3>
                                                <h4>2018.05.24 <span>Cloud</span></h4>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="related-item">
                                            <div class="related-img">
                                                <img src="<?php echo get_template_directory_uri()?>/assets/img/img_blogdetail01.jpg" alt="" class="is-wide">
                                            </div>
                                            <div class="related-cont">
                                                <h3>「第13回 CTC SaaS Day」について RPA（S/Wロボット）の活用により実現する働き方改革 ～</h3>
                                                <h4>2018.05.24 <span>Cloud</span></h4>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="md-4 xs-12">
                        <?php get_sidebar(); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php get_footer(); ?>