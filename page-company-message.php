<?php
	/***
		Template Name: Company Message
	***/
?>
<?php get_header(); ?>

    <section class="wrp sec-company">
        <div class="page-title titlev1">
            <div class="cntr">
                <h2>
                    Company <span>社長からのメッセージ</span>
                </h2>
            </div>
        </div>
        <div class="breadcrumbs">
            <div class="cntr">
                <ul>
                    <li>
                        <a href="<?php bloginfo('url'); ?>">Home</a>
                    </li>
                    <li>
                        <a href="<?php bloginfo('url'); ?>/company">会社概要</a>
                    </li>
                    <li>
                        <span>社長からのメッセージ</span>
                    </li>
                </ul>
            </div>
        </div>
        <div class="company-menu">
            <div class="cntr">
                <?php 
					wp_nav_menu(
						array (
							'theme_location' => 'third',
							'walker'         => new WPSE_78121_Sublevel_Walker
							)
						); 
				?>
            </div>
        </div>
        <div class="company-box">
            <div class="cntr">
                <div class="company-content">
                    <h2 class="title">
                        <em>TOP MESSAGE</em>
                        <span>社長からのメッセージ</span>
                    </h2>
                    <div class="company-message-top">
                        <img src="<?php echo get_template_directory_uri()?>/assets/img/img_company04.jpg" alt="" class="is-wide v-pc">
                        <img src="<?php echo get_template_directory_uri()?>/assets/img/img_company04_sp.jpg" alt="" class="is-wide v-sp">
                        <div class="company-message-name">
                            <h4>
                                株式会社ビッグツリーテクノロジー&コンサルティング<br>
                                代表取締役社長 <span>杉山 健</span>
                            </h4>
                        </div>
                    </div>
                    <div class="company-message-cont">
                        <div class="gap gap-30 gap-0-xs aggresive">
                            <div class="md-7 xs-12">
                                <h2 class="title titlev2">
                                    「攻めのIT」で新しい価値を創造し続ける
                                </h2>
                                <p>
                                    我々BTCは2002年の創業以来、弊社が持つ「Technology」と「Consulting」スキルを融合さ
                                    せることで、弊社のビジョンである「文化の創造」を実現すべく、活動してきました。
                                    おかげ様で、様々な業界のリーディングカンパニー様とともに、プロジェクトを推進させて頂き、
                                    「文化の創造」に資する「攻めのIT」を実現する会社として高い評価を頂いております。
                                    Technologyの進化はとどまることを知らず、さらに加速している状況であり、弊社の唱える
                                    「Technologyの進化による文化革命」が起こっていることを実感しております。<br><br>
                                    このような状況において、弊社は「攻めのIT」を更に強力に推し進めていくために必要な「Digital」
                                    領域の強化をしております。<br>
                                    「Digital」においては、マーケティング戦略の立案からUI/UXデザイン、サイト構築まで、クライア
                                    ントのインターネット事業をトータルでサポートしています。
                                </p>
                            </div>
                            <div class="md-5 xs-12">
                                <img src="<?php echo get_template_directory_uri()?>/assets/img/img_company05.jpg" alt="" class="is-wide v-pc">
                                <img src="<?php echo get_template_directory_uri()?>/assets/img/img_company05_sp.jpg" alt="" class="is-wide v-sp">
                            </div>
                        </div>
                        <div class="gap gap-30 gap-0-xs">
                            <div class="md-5 xs-12">
                                <img src="<?php echo get_template_directory_uri()?>/assets/img/img_company06.jpg" alt="" class="is-wide v-pc">
                            </div>
                            <div class="md-7 xs-12">
                                <p>
                                    また、昨今の労働人口の減少を背景としたホワイトカラーの生産性を向上させるための施策とし
                                    て「RPA(Robotics Process Automation)」領域も強化しております。
                                    「RPA」は仮想知的労働者(Digital Labor)とも言われ、これまでとは異なるレベルで「爆発的な
                                    生産性向上」を図る可能性を秘めており、来るべきAI技術の業務適用第1段階としてとらえられて
                                    います。<br><br>
                                    BTCにおいては、創業以来こだわっている「Consulting」スキルをベースとして、「Digital」×「Tec
                                    hnology」×「RPA」を核としたサービスラインにて新しい価値を創造し、真の成功を目指すお客
                                    様のパートナーであり続けます。
                                </p>
                            </div>
                            <div class="xs-12 v-sp mt-10-xs">
                                <img src="<?php echo get_template_directory_uri()?>/assets/img/img_company06_sp.jpg" alt="" class="is-wide">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php get_footer(); ?>