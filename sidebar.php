<div class="sidebar-box">
    <div class="sidebar-wrp">
        <h3 class="sidebar-tit">人気記事</h3>
        <ul class="article-lists">
            <?php 
            $popularpost = new WP_Query( array( 'posts_per_page' => 6, 'meta_key' => 'wpb_post_views_count', 'orderby' => 'meta_value_num', 'order' => 'DESC'  ) );
            while ( $popularpost->have_posts() ) : $popularpost->the_post(); ?>
            
            <li>
                <a href="<?php the_permalink(); ?>" class="article-item">
                    <div class="article-img">
                    <?php
                        $thumb_id = get_post_thumbnail_id(get_the_ID());
                        $alt = get_post_meta($thumb_id, '_wp_attachment_image_alt', true);
                    ?>
                    <img title="<?php the_title(); ?>" alt="<?php echo $alt; ?>" class="wp-post-image is-wide" src="<?=wp_get_attachment_url( get_post_thumbnail_id() ); ?>">
                    </div>
                    <div class="article-tit">
                        <?php echo mb_strimwidth(get_the_title(), 0, 50, '...'); ?>
                    </div>
                </a>
            </li>
            
            <?php endwhile;
            ?>
        </ul>
    </div>
    <div class="sidebar-wrp">
        <h3 class="sidebar-tit">カテゴリー</h3>
        <?php
        $cat_args = array('orderby' => 'name', 'show_count' => '1', 'hierarchical' => '0','taxonomy' => 'core-technology_cat');?>
        <?php
        $cat_args2 = array('orderby' => 'name', 'show_count' => '1', 'hierarchical' => '0','taxonomy' => 'category');?>
        <ul class="category-lists">
            <?php
                    $cat_args['title_li'] = '';
                    wp_list_categories(apply_filters('', $cat_args));
            ?>
            <?php
                    $cat_args2['title_li'] = '';
                    wp_list_categories(apply_filters('', $cat_args2));
            ?>
        </ul>
    </div>
    <div class="sidebar-wrp">
        <h3 class="sidebar-tit">アーカイブ</h3>
        <ul class="category-lists">
            <?php 
                $args = array(
                    'type'              => 'yearly',
                    'show_post_count'   => true
                );
                wp_get_archives($args);
            ?>
        </ul>
    </div>
</div>