<?php
	/***
		Template Name: Company Office Tour
	***/
?>
<?php get_header(); ?>

<section class="wrp sec-company">
        <div class="page-title titlev1">
            <div class="cntr">
                <h2>
                    Company <span>オフィスツアー</span>
                </h2>
            </div>
        </div>
        <div class="breadcrumbs">
            <div class="cntr">
                <ul>
                    <li>
                        <a href="<?php bloginfo('url'); ?>">Home</a>
                    </li>
                    <li>
                        <a href="<?php bloginfo('url'); ?>/company">会社概要</a>
                    </li>
                    <li>
                        <span>オフィスツアー</span>
                    </li>
                </ul>
            </div>
        </div>
        <div class="company-menu">
            <div class="cntr">
                <?php 
					wp_nav_menu(
						array (
							'theme_location' => 'third',
							'walker'         => new WPSE_78121_Sublevel_Walker
							)
						); 
				?>
            </div>
        </div>
        <div class="company-box">
            <div class="cntr">
                <div class="company-content">
                    <h2 class="title">
                        <em>OFFICE TOUR</em>
                        <span>オフィスツアー</span>
                    </h2>
                    <div class="company-office-tour">
                        <div class="floor-img">
                            <img src="<?php echo get_template_directory_uri()?>/assets/img/pic_tour_floorview.png" alt="" class="is-wide for-pc">
                            <img src="<?php echo get_template_directory_uri()?>/assets/img/pic_tour_floorview_sp.png" alt="" class="for-sp">
                        </div>
                        <ul class="floor-list">
                            <li class="entrance">
                                <a href="#">
                                    <img src="<?php echo get_template_directory_uri()?>/assets/img/img_tour_entrance.jpg" alt="" class="is-wide">
                                </a>
                            </li>
                            <li class="reception">
                                <a href="#">
                                    <img src="<?php echo get_template_directory_uri()?>/assets/img/img_tour_reception.jpg" alt="" class="is-wide">
                                </a>
                            </li>
                            <li class="waiting-space">
                                <a href="#">
                                    <img src="<?php echo get_template_directory_uri()?>/assets/img/img_tour_watingspace.jpg" alt="" class="is-wide">
                                </a>
                            </li>
                            <li class="meeting-room01">
                                <a href="#">
                                    <img src="<?php echo get_template_directory_uri()?>/assets/img/img_tour_meetingroom01.jpg" alt="" class="is-wide">
                                </a>
                            </li>
                            <li class="meeting-room02">
                                <a href="#">
                                    <img src="<?php echo get_template_directory_uri()?>/assets/img/img_tour_meetingroom02.jpg" alt="" class="is-wide">
                                </a>
                            </li>
                            <li class="meeting-room03">
                                <a href="#">
                                    <img src="<?php echo get_template_directory_uri()?>/assets/img/img_tour_meetingroom03.jpg" alt="" class="is-wide">
                                </a>
                            </li>
                            <li class="meeting-room04">
                                <a href="#">
                                    <img src="<?php echo get_template_directory_uri()?>/assets/img/img_tour_meetingroom04.jpg" alt="" class="is-wide">
                                </a>
                            </li>
                            <li class="refresh-room01">
                                <a href="#">
                                    <img src="<?php echo get_template_directory_uri()?>/assets/img/img_tour_refreshroom01.jpg" alt="" class="is-wide">
                                </a>
                            </li>
                            <li class="refresh-room02">
                                <a href="#">
                                    <img src="<?php echo get_template_directory_uri()?>/assets/img/img_tour_refreshroom02.jpg" alt="" class="is-wide">
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="company-modal">
        <div class="cntr">
            <div class="company-modal-box">
                <div class="modal-close"><span>x</span></div>
                <div class="company-modal-img owl-carousel owl-theme" id="modal-carousel">
                    <!-- <div class="company-modal-title">Meeting Room 01</div> -->
                    <img src="<?php echo get_template_directory_uri()?>/assets/img/img_tour_entrance.jpg" alt="" class="is-wide">
                </div>
            </div>
        </div>
    </div>

<?php get_footer(); ?>
