<?php get_header(); ?>

    <section class="wrp sec-blog">
        <div class="breadcrumbs">
            <div class="cntr">
                <ul>
                    <li>
                        <a href="<?php bloginfo('url'); ?>">Home</a>
                    </li>
                    <li>
                        <a href="<?php bloginfo('url'); ?>/news">News</a>
                    </li>
                    <li>
                        <span><?php the_title(); ?></span>
                    </li>
                </ul>
            </div>
        </div>
        
        <div class="blog-details">
            <div class="cntr">
                <div class="gap gap-30 gap-0-xs">
                    <div class="md-8 xs-12">
                        <div class="blog-detail-box">
                        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                            <div class="blog-detail-header">
                                <h2 class="blog-detail-title"><?php the_title(); ?></h2>
                                <div class="blog-detail-date-cat">
                                    <h4><?php the_date('Y.m.d'); ?> <span><?php $cat = get_the_category(); echo $cat[0]->cat_name; ?></span></h4>
                                </div>
                            </div>
                            <div class="blog-detail-body">
                                <div class="blog-detail-img">
                                    <?php
										$thumb_id = get_post_thumbnail_id(get_the_ID());
										$alt = get_post_meta($thumb_id, '_wp_attachment_image_alt', true);
									?>
									<img title="<?php the_title(); ?>" alt="<?php echo $alt; ?>" class="wp-post-image is-wide" src="<?=wp_get_attachment_url( get_post_thumbnail_id() ); ?>">
                                </div>
                                <div class="blog-detail-cont">
                                    <?php the_content(); ?>
                                </div>
                                <div class="social-media-icon">
                                    <?php wcr_share_buttons(); ?>
                                </div>
                            </div>
                            <?php endwhile; ?>
                            <?php endif; ?>
                            <div class="blog-detail-footer">
                                <ul class="blog-related-list">
                                    <?php
                                    $related = get_posts( array( 'category__in' => wp_get_post_categories($post->ID), 'numberposts' => 2, 'post__not_in' => array($post->ID) ) );
                                    if( $related ) foreach( $related as $post ) {
                                    setup_postdata($post); ?>
                                        <li>
                                        <a href="<?php the_permalink() ?>" class="related-item">
                                            <div class="related-img">
                                            <?php
                                                $thumb_id = get_post_thumbnail_id(get_the_ID());
                                                $alt = get_post_meta($thumb_id, '_wp_attachment_image_alt', true);
                                            ?>
                                            <img title="<?php the_title(); ?>" alt="<?php echo $alt; ?>" class="wp-post-image is-wide" src="<?=wp_get_attachment_url( get_post_thumbnail_id() ); ?>">
                                            </div>
                                            <div class="related-cont">
                                                <h3><?php the_title(); ?></h3>
                                                <h4><?php the_date('Y.m.d'); ?> <span><?php $cat = get_the_category(); echo $cat[0]->cat_name; ?></span></h4>
                                            </div>
                                        </a>
                                        </li>
                                    <?php }
                                    wp_reset_postdata(); ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="md-4 xs-12">
                        <?php get_sidebar(); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php get_footer(); ?>