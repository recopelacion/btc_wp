<?php get_header(); ?>

<section class="wrp search-result">
    <div class="cntr plr-0-xs">
        <div class="gap">
            <div class="search-result-wrp">
                <?php if( have_posts() ) : ?>
                    <?php while( have_posts() ) : the_post(); ?>
                        <?php if( isset($_GET['post_type']) ){
                            $type = $_GET['post_type'];
                            if($type == 'posts') {?>
                            <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                                <div class="search-result-box">
                                    <a href="#">
                                        <div class="img">
                                        <?php
                                            $thumb_id = get_post_thumbnail_id(get_the_ID());
                                            $alt = get_post_meta($thumb_id, '_wp_attachment_image_alt', true);
                                        ?>
                                        <img title="<?php the_title(); ?>" alt="<?php echo $alt; ?>" class="wp-post-image" src="<?=wp_get_attachment_url( get_post_thumbnail_id() ); ?>">
                                        </div>
                                        <div class="title"><?php the_title(); ?></div>
                                        <div class="content"><?php the_content(); ?></div>
                                    </a>
                                </div>
                            </div>
                        <?php } else { ?>
                            <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                                <div class="search-result-box">
                                    <a href="#">
                                        <div class="img">
                                        <?php
                                            $thumb_id = get_post_thumbnail_id(get_the_ID());
                                            $alt = get_post_meta($thumb_id, '_wp_attachment_image_alt', true);
                                        ?>
                                        <img title="<?php the_title(); ?>" alt="<?php echo $alt; ?>" class="wp-post-image" src="<?=wp_get_attachment_url( get_post_thumbnail_id() ); ?>">
                                        </div>
                                        <div class="title"><?php the_title(); ?></div>
                                        <div class="content"><?php the_content(); ?></div>
                                    </a>
                                </div>
                            </div>
                            <?php } ?>
                        <?php } else { ?>
                            <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                                <div class="search-result-box">
                                    <a href="#">
                                        <div class="img">
                                        <?php
                                            $thumb_id = get_post_thumbnail_id(get_the_ID());
                                            $alt = get_post_meta($thumb_id, '_wp_attachment_image_alt', true);
                                        ?>
                                        <img title="<?php the_title(); ?>" alt="<?php echo $alt; ?>" class="wp-post-image" src="<?=wp_get_attachment_url( get_post_thumbnail_id() ); ?>">
                                        </div>
                                        <div class="title"><?php the_title(); ?></div>
                                        <div class="content"><?php the_content(); ?></div>
                                    </a>
                                </div>
                            </div>
                            <?php } ?>
                    <?php endwhile; else: ?>
                    <h4 class="pb-no-results">No Results Found for "<?php echo esc_html( get_search_query( false ) ); ?>"</h4>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>