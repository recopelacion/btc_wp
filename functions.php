<?php

/*
 * ----------------------------------------------------------------------------------------
 *  GET TEMPLATE DIRECTORY
 * ----------------------------------------------------------------------------------------
 */
require get_template_directory() . '/inc/enqueue.php';
require get_template_directory() . '/inc/theme-support.php';
require get_template_directory() . '/inc/cleanup.php';
require get_template_directory() . '/inc/custom-post.php';
require get_template_directory() . '/inc/custom-taxonomy.php';
require get_template_directory() . '/inc/shortcode.php';
require get_template_directory() . '/inc/custom-fields.php';