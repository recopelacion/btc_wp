(function($){

	var $floor_list = $('.floor-list'),
		$f_i = 0,
		$floor_count = $floor_list.find('li').length;

	for($f_i; $f_i < $floor_count; $f_i++) {
		$floor_list.find('li:eq('+$f_i+')').append("<div class='list-num'><span>"+ parseInt(parseInt($f_i)+1) +"</span></div>");
	}

	// toggle nav
	var $nav_toggle = $('#toggle-nav'),
		$m_nav = $('.main-nav');

	$nav_toggle.click(function(e){
		e.preventDefault();
		
		if($(this).hasClass('is-on')) {
			$(this).removeClass('is-on');
			$m_nav.removeClass('is-translated');
		}else{
			$(this).addClass('is-on');
			$m_nav.addClass('is-translated');
		}

		return false;
	});
	
	var $toTop = $('#toTop');

	$toTop.click(function(e){
		$('body,html').animate({
			scrollTop: 0},500);
		return false;
	});	
	
	var $parent_sub = $('.menu-item-has-children');
	$parent_sub.click(function(e){
		e.preventDefault();

		if($(this).hasClass('is-toggled') || $(window).innerWidth() > 750) {
			$(this).removeClass('is-toggled');
		}else{
			$(this).addClass('is-toggled');
		}
		if($(window).innerWidth() < 751) {
			$(this).find('.sub-menu').slideToggle();
			$parent_sub.not($(this)).removeClass('is-toggled');
			$parent_sub.find('.sub-menu').not($(this).find('.sub-menu')).slideUp();
		}
	});

}) (jQuery);


jQuery(document).ready(function($) {
	$(window).on('load resize', function(){

		var $parent_sub = $('.menu-item-has-children'),
			$header = $('.head');	
		
			$('body').css({
				marginTop: $header.innerHeight() + 'px'
			});

			$('.has-slider .owl-nav').css({
				top: $('.has-slider').find('.item-img').innerHeight() / 2.2 +'px'
			});

		if($(window).innerWidth() < 751) {

			$('.has-slider .owl-nav').css({
				top: $('.has-slider').find('.item-img').innerHeight() / 2.3 +'px'
			});
			
			// if($(window).height() > 650) {
			// 	$('.hero').css({
			// 		height: $(window).height() / 1.5 + 'px'
			// 	});
			// }else{
			// 	$('.hero').css({
			// 		height: $(window).height() - $('.head').height() + 'px'
			// 	});
			// }
			
		}else{
			$parent_sub.removeClass('is-toggled');
			$parent_sub.find('.sub-menu').fadeIn();
		}
	});
});


AOS.init();
(function() {
	var $col_Slider = $('.column-slider');
	$col_Slider.owlCarousel({
		items: 1,
		lazyLoad: true,
		autoplay: true,
		center: true,
		nav: true,
		loop: true,
		margin: 0,
		padding: 0,
		pullDrag: true,
		touchDrag: true,
		mouseDrag: true,
		autoplayHoverPause: true,
		itemsScaleUp: false,
		slideSpeed: 300
	});

	$(window).on('load resize',function(){
		if($(window).innerWidth() < 751) {
			$('#news-section').addClass("owl-carousel owl-theme");
			$('#news-section').owlCarousel({
				items: 1,
				lazyLoad: true,
				autoplay: false,
				center: true,
				dots: true,
				loop: true,
				margin: 15,
				padding: 0,
				pullDrag: true,
				touchDrag: true,
				mouseDrag: true,
				responsiveClass: true,
				responsive: {
					0: {
						items: 1
					},
					500: {
						items: 2
					}
				},
				itemsScaleUp: false,
				slideSpeed: 300
			});
		}else{
			$('#news-section').removeClass("owl-carousel owl-theme");
			$('#news-section').trigger('destroy.owl.carousel');
		}
	})
})(jQuery);

$(function() {
	$('.search-btn').on("click", function(event) {
		event.preventDefault();
		$("body").addClass("open");
		// $('.search').focus();
	});

	$("body, button.close").on("click keyup", function(event) {
		if (
		event.target == this ||
		event.target.className == "close" ||
		event.keyCode == 27
		) {
		$(this).removeClass("open");
		}
	});

	// $("form").submit(function(event) {
	// 	event.preventDefault();
	// 	return false;
	// });
});


jQuery(function($) {
    $('.main-slider-box').slick({
        slidesToShow: 1,
		slidesToScroll: 1,
		speed: 1000,
        autoplay: true,
        autoplaySpeed: 5000,
        responsive: [
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: true
            }
        },
        {
            breakpoint: 767,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
        ]
	});

	$('.main-slider-box').mouseover(function() {
		$('.main-slider-box').slick('slickPlay')
	});

	$('.btn-exec').click(function(e) {
		e.preventDefault();
		$('.btn-exec').toggleClass('show');
		$(this).next("div").slideToggle('slow');
  	});

});

