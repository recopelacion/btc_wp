<?php get_header(); ?>

    <section class="wrp sec-blog">
        <div class="breadcrumbs">
            <div class="cntr">
                <ul>
                    <li>
                        <a href="<?php bloginfo('url'); ?>">Home</a>
                    </li>
                    <li>
                        <a href="<?php bloginfo('url'); ?>/core-technology">Core Technology</a>
                    </li>
                    <li>
                        <span><?php the_title(); ?></span>
                    </li>
                </ul>
            </div>
        </div>
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        <div class="blog-details">
            <div class="cntr">
                <div class="gap gap-30 gap-0-xs">
                    <div class="md-8 xs-12">
                        <div class="blog-detail-box">
                            <div class="blog-detail-header">
                                <h2 class="blog-detail-title"><?php the_title(); ?></h2>
                                <div class="blog-detail-date-cat">
                                    <h4><?php the_date('Y.m.d'); ?> <span>
                                    <?php
                                        $terms = get_the_terms( $post->ID , 'core-technology_cat' );

                                        foreach ( $terms as $term ) {
                                        
                                        echo $term->name;
                                        
                                        }
                                    ?>
                                    </span></h4>
                                </div>
                            </div>
                            <div class="blog-detail-body">
                                <div class="blog-detail-img">
                                    <?php
										$thumb_id = get_post_thumbnail_id(get_the_ID());
										$alt = get_post_meta($thumb_id, '_wp_attachment_image_alt', true);
									?>
									<img title="<?php the_title(); ?>" alt="<?php echo $alt; ?>" class="wp-post-image is-wide" src="<?=wp_get_attachment_url( get_post_thumbnail_id() ); ?>">
                                </div>
                                <div class="blog-detail-cont">
                                    <?php the_content(); ?>
                                </div>
                                <div class="social-media-icon">
                                    <?php wcr_share_buttons(); ?>
                                </div>
                            </div>
                            <div class="blog-detail-footer">
                                <ul class="blog-related-list">
                                    <?php
                                    //Get array of terms
                                        $terms = get_the_terms( $post->ID , 'core-technology_cat', 'string');
                                        //Pluck out the IDs to get an array of IDS
                                        $term_ids = wp_list_pluck($terms,'term_id');

                                        //Query posts with tax_query. Choose in 'IN' if want to query posts with any of the terms
                                        //Chose 'AND' if you want to query for posts with all terms
                                        $second_query = new WP_Query( array(
                                            'post_type' => 'core-technology',
                                            'tax_query' => array(
                                                            array(
                                                                'taxonomy' => 'core-technology_cat',
                                                                'field' => 'id',
                                                                'terms' => $term_ids,
                                                                'operator'=> 'IN' //Or 'AND' or 'NOT IN'
                                                            )),
                                            'posts_per_page' => 2,
                                            'ignore_sticky_posts' => 1,
                                            'orderby' => 'rand',
                                            'post__not_in'=>array($post->ID)
                                        ) );

                                        //Loop through posts and display...
                                            if($second_query->have_posts()) {
                                            while ($second_query->have_posts() ) : $second_query->the_post(); ?>
                                            <li>
                                                <a href="<?php the_permalink() ?>" class="related-item">
                                                    <div class="related-img">
                                                    <?php
                                                        $thumb_id = get_post_thumbnail_id(get_the_ID());
                                                        $alt = get_post_meta($thumb_id, '_wp_attachment_image_alt', true);
                                                    ?>
                                                    <img title="<?php the_title(); ?>" alt="<?php echo $alt; ?>" class="wp-post-image is-wide" src="<?=wp_get_attachment_url( get_post_thumbnail_id() ); ?>">
                                                    </div>
                                                    <div class="related-cont">
                                                        <h3><?php the_title(); ?></h3>
                                                        <h4><?php the_date('Y.m.d'); ?> <span>
                                                        <?php
                                                                    $terms = get_the_terms( $post->ID , 'core-technology_cat' );

                                                                    foreach ( $terms as $term ) {
                                                                    
                                                                    echo $term->name;
                                                                    
                                                                    }
                                                                ?>
                                                        </span></h4>
                                                    </div>
                                                </a>
                                            </li>
                                        <?php endwhile; wp_reset_query();
                                        }
                                        ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="md-4 xs-12">
                        <?php get_sidebar(); ?>
                    </div>
                </div>
            </div>
        </div>
        <?php endwhile; ?>
        <?php endif; ?>
    </section>
<?php get_footer(); ?>