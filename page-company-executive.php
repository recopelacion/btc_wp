<?php
	/***
		Template Name: Company Executive
	***/
?>
<?php get_header(); ?>

<section class="wrp sec-company">
        <div class="page-title titlev1">
            <div class="cntr">
                <h2>
                    Company <span>役員紹介</span>
                </h2>
            </div>
        </div>
        <div class="breadcrumbs">
            <div class="cntr">
                <ul>
                    <li>
                        <a href="<?php bloginfo('url'); ?>">Home</a>
                    </li>
                    <li>
                        <a href="<?php bloginfo('url'); ?>/company">会社概要</a>
                    </li>
                    <li>
                        <span>役員紹介</span>
                    </li>
                </ul>
            </div>
        </div>
        <div class="company-menu">
            <div class="cntr">
                <?php 
					wp_nav_menu(
						array (
							'theme_location' => 'third',
							'walker'         => new WPSE_78121_Sublevel_Walker
							)
						); 
				?>
            </div>
        </div>
        <div class="company-box">
            <div class="cntr">
                <div class="company-content">
                    <h2 class="title">
                        <em>BTC’S EXECUTIVE</em>
                        <span>役員紹介</span>
                    </h2>
                    <div class="company-members">
                        <div class="gap gap-15 gap-0-xs">
                            <?php
                                if ( get_query_var('paged') ) { $paged = get_query_var('paged'); } else if ( get_query_var('page') ) {$paged = get_query_var('page'); } else {$paged = 1; }
                                $temp = $wp_query;
                                $wp_query = null;
                                $args = array( 'post_type' => 'member', 'order'=>'DESC', 'posts_per_page' => -1, 'paged' => $paged);
                                $wp_query = new WP_Query();
                                $wp_query->query( $args );
                                while ($wp_query->have_posts()) : $wp_query->the_post();
                            ?>
                            <div id="post-<?php the_ID(); ?>" <?php post_class('md-4 xs-12'); ?>>
                                <div class="member-box">
                                    <div class="members-img">
                                        <?php
                                            $thumb_id = get_post_thumbnail_id(get_the_ID());
                                            $alt = get_post_meta($thumb_id, '_wp_attachment_image_alt', true);
                                        ?>
                                        <img title="<?php the_title(); ?>" alt="<?php echo $alt; ?>" class="wp-post-image is-wide" src="<?=wp_get_attachment_url( get_post_thumbnail_id() ); ?>">
                                    </div>
                                    <div class="members-details tc">
                                        <div class="members-name">
                                            <h3><?php echo get_post_meta($post->ID,'jp_name', true); ?></h3>
                                            <h4><?php the_title(); ?></h4>
                                        </div>
                                        <div class="btn btnv2 btn-more btn-exec">
                                            <a href="javascript:void(0);" class="toggle-collapse">
                                                More
                                            </a>
                                        </div>
                                        <div class="members-cont">
                                            <?php the_content(); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php endwhile; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php get_footer(); ?>